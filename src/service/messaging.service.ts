import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { BehaviorSubject } from 'rxjs'
@Injectable()
export class MessagingService {
currentMessage = new BehaviorSubject(null);
    constructor(private angularFireMessaging: AngularFireMessaging) {
    this.angularFireMessaging.messaging.subscribe(
          (_messaging) => {
          _messaging.onMessage = _messaging.onMessage.bind(_messaging);
          _messaging.onTokenRefresh = _messaging.onTokenRefresh.bind(_messaging);
          }
          )
          }
          requestPermission() {
          this.angularFireMessaging.requestToken.subscribe(
          (token) => {
      //     console.log("TOKEN",token);
          },
          (err) => {
          console.error('Unable to get permission to notify.', err);
          }
          );
          }
          

         // curl https://fcm.googleapis.com/fcm/send --header "Authorization:key=[TU_KEY]" --header "Content-Type:application/json" -d '{ "notification": { "title": "Hey amigo, lee esto!", "body": "Felicidades!! Has recibido una gloriosa notificación", "icon": "/notificaciones/images/user-icon.png" }, "to" : "[dOF-VXIuRpuLd6PDJ_4ixw:APA91bES7vF9ka5bu0g7CMF0vw6bPWOjRH8JDaF9jlXEa1K6Fz8CyAWnI8zrCEy809m-JV2OsX_nt57KZHrF7TtIaajpd4RnEbXFPZGCzJjtCwdQIsDP9K2FYYDgO2kwZ1Mz1UK-bHB6]" }'



          receiveMessage() {
          this.angularFireMessaging.messages.subscribe(
          (payload) => {
          console.log("new message received. ", payload);
          this.currentMessage.next(payload);
          })
    }
    }