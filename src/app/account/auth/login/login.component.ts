import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
import { MessagingService } from '../../../../service/messaging.service';
//MENSAJE PUSH

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/**
 * Login component
 */
export class LoginComponent implements OnInit, AfterViewInit {

  loginForm: FormGroup;
  submitted = false;
  error = '';
  returnUrl: string;

  // set the currenr year
  year: number = new Date().getFullYear();
  title = 'push-notification';
  message;
  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private db: DatabaseService,
              private spinner: NgxSpinnerService,
              private authenticated: AuthenticatedService,
              private router: Router,
              private messagingService: MessagingService) { }

  ngOnInit() {
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage;

    this.loginForm = this.formBuilder.group({
      email: ['c.sanchez@vinarytech.com', [Validators.required, Validators.email]],
      password: ['123456789', [Validators.required]],
    });

    // reset login status
    // this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields

  /**
   * Form submit
   */
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    } else {
      this.spinner.show();
      this.db.col$('Administradores').subscribe((usuarios: any) => {
        for (const usuario of usuarios) {
          if (this.loginForm.controls.email.value === usuario.email && this.loginForm.controls.password.value === usuario.password) {
            this.spinner.hide();
            this.authenticated.setCurrentUser(usuario);
            Swal.fire({
              icon: 'success',
              title: 'OK',
              text: 'Ingreso correcto',
              showConfirmButton: false,
              timer: 500
            });
            this.router.navigate(['/dashboard']);
            return;
          }
        }
        this.spinner.hide();
        Swal.fire({
          icon: 'warning',
          title: 'Usuario no existe',
          text: 'Email o contraseña incorrectos',
        });
      }, error => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Revisa tu conexión a internet',
        });
      });
    }
  }
}
