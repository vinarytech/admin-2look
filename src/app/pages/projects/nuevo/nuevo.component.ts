import { Component, OnInit, Input, EventEmitter, ViewChild, Output } from '@angular/core';

//import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
//import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
// AÑADIR PARA AGREGAR USUARIO
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
//import { ApiService } from '../../../core/services/api.service';
import { Subcribe, User } from '../../../core/models/auth.models';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DatabaseService } from '../../../services/database.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from 'angularfire2/storage';
import * as moment from 'moment/moment';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
import { timer } from 'rxjs';
//import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.scss']
})
export class NuevoComponent implements OnInit {

  UserForm = new FormGroup({
    nombre: new FormControl('', [Validators.required, Validators.minLength(5)]),
    correo: new FormControl('', [Validators.required, Validators.required]),
    contrasenia: new FormControl('', [Validators.required, Validators.minLength(4)]),
    tipo: new FormControl('', [Validators.required]),
    celular: new FormControl('', [Validators.required]),
    fecha: new FormControl(),
    cedula: new FormControl('', [Validators.required]),
    //PROFESIONAL
    nombreEmpresa: new FormControl('', [Validators.required]),
    anosExperiencia: new FormControl('', [Validators.minLength(1)]),
    recomendado: new FormControl(false),
    estrellas: new FormControl('', [Validators.required]),
    //INF. OCULTA
    id_user_action: new FormControl('', [Validators.required]),
    token: new FormControl('', [Validators.required]),
    verif_cedula: new FormControl(true),
    verif_celular: new FormControl(false),
    verif_correo: new FormControl(false),
    bloqueado: new FormControl(false),
    //status:new FormControl(true),
  });
  newUser = {
    nombre: '',
    correo:'',
    contrasenia:'',
    tipo:'',
    celular:'',
    fecha: moment().format('YYYY-MM-DD'),
    cedula: '',
    nombreEmpresa: '',
    anosExperiencia: '',
    recomendado: false,
    estrellas: '',
    id_user_action:this.authenticated.currentUser.id,
    verif_cedula:true,
    verif_celular:false,
    verif_correo:false,
    bloqueado:false,
    status:true,

  };
  constructor( private authenticated: AuthenticatedService,private storage: AngularFireStorage,private formBuilder: FormBuilder,private asFT: AngularFireStorage ,private db: DatabaseService, private spinner: NgxSpinnerService,private router: Router) { 
    
  }
  breadCrumbItems: Array<{}>;

  // hoveredDate: NgbDate;
  // fromNGDate: NgbDate;
  // toNGDate: NgbDate;
  selected: any;
  hidden: boolean;
  fileData: File=null;
  previewUrlLicencia:any=null;
  fotografia:any;
  
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  editar = false;
  licencia: any;
  currentUser: any;
  cardImageBase64: string;
  isImageSaved: boolean;
  imageError: string;
  uri: any;
  aux:any;
  previewUrlCedula: any = null;
  previewCedula: any = null;
  imgBase64Path: any;


  hoveredDate: NgbDate;
   fromNGDate: NgbDate;
  toNGDate: NgbDate;

  
  // selected: any;
  // hidden: boolean;

  @Input() fromDate: Date;
  @Input() toDate: Date;
  @Output() dateRangeSelected: EventEmitter<{}> = new EventEmitter();

  @ViewChild('dp', { static: true }) datePicker: any;

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Aplicación' }, { label: 'Nuevo usuario', active: true }];
    this.selected = '';
    this.hidden = true;
    this.spinner.show();
    this.spinner.hide();
  }
  fileProgress(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          this.newUser.cedula = e.target.result;
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }
//INPUT
fileProgressLicencia(fileInput: any) {
  this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/png', 'image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];
          this.imgBase64Path = e.target.result;
          this.cardImageBase64 = this.imgBase64Path;
          this.isImageSaved = true;
          this.UserForm.patchValue({fotografia: this.imgBase64Path})
          console.log(this.imgBase64Path);
                 this.aux = true;
          // this.previewImagePath = imgBase64Path;

        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
      this.fileData = <File>fileInput.target.files[0];
      this.previewCedula();
    }
}

previewLicencia() {
  // Show preview 
  var mimeType = this.fileData.type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }

  var reader = new FileReader();
  reader.readAsDataURL(this.fileData);
  reader.onload = (_event) => {
    this.previewUrlLicencia = reader.result;
    console.log(this.previewLicencia);
    console.log(this.fileData);
    
    this.UserForm.patchValue({fotografia: this.fileData})
  }
}

ObtenerFoto(){
  const randomId = Math.random().toString(36).substring(2, 8);
    const path = `Administradores/${new Date().getTime()}_${randomId}`; // full path file
    const ref = this.asFT.ref(path);
    ref.putString(this.fotografia, 'data_url').then(() => {
      const url = ref.getDownloadURL();
      url.subscribe((url) => {
        console.log(url);
          this.UserForm.patchValue({fotografia: url})
      });
    }).catch(error => {
     // this.alerts.presentToast(error.message, 'danger');
    });
}



  files: File[] = [];

	onSelect(event:any) {
    console.log(event);
    
    // if(this.files && this.files.length >=2) {
    //   this.onRemove(this.files[0]);
    // }
    // this.files.push(...event.addedFiles);
    
  }
   
  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  // buildForm() {
  //   this.UserForm = this.formBuilder.group({
  //     nombre: ['', [Validators.required,  Validators.minLength(5)]],
  //     email: ['', [Validators.required, Validators.email]],
  //     password: ['', [Validators.required,  Validators.minLength(5)]],
  //     rol: ['', [Validators.required,  Validators]],
  //   });
  // }

  clearForm() {
    this.UserForm.patchValue({ nombre: '' });
    this.UserForm.patchValue({ nombreEmpresa: '' });
    this.UserForm.patchValue({ anosExperiencia: '' });
    this.UserForm.patchValue({ estrellas: '' });
    // this.UserForm.patchValue({ email: '' });
    // this.UserForm.patchValue({ password: '' });
    
  }

  onSubmit() {

    
    this.db.add('Usuarios',this.UserForm.value);
    
   this.router.navigate(['/tables/advanced']);
   // this.ngOnInit();
   
  }


  

  /**
   * }
   * on date selected
   * @param date date object
   */
  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromNGDate = date;
      this.fromDate = new Date(date.year, date.month - 1, date.day);
      this.selected = '';
    } else if (this.fromDate && !this.toDate && date.after(this.fromNGDate)) {
      this.toNGDate = date;
      this.toDate = new Date(date.year, date.month - 1, date.day);
      this.hidden = true;
      this.selected = this.fromDate.toLocaleDateString() + '-' + this.toDate.toLocaleDateString();

      this.dateRangeSelected.emit({ fromDate: this.fromDate, toDate: this.toDate });

      this.fromDate = null;
      this.toDate = null;
      this.fromNGDate = null;
      this.toNGDate = null;

    } else {
      this.fromNGDate = date;
      this.fromDate = new Date(date.year, date.month - 1, date.day);
      this.selected = '';
    }
  }
  /**
   * Is hovered over date
   * @param date date obj
   */
  isHovered(date: NgbDate) {
    return this.fromNGDate && !this.toNGDate && this.hoveredDate && date.after(this.fromNGDate) && date.before(this.hoveredDate);
  }

  /**
   * @param date date obj
   */
  isInside(date: NgbDate) {
    return date.after(this.fromNGDate) && date.before(this.toNGDate);
  }

  /**
   * @param date date obj
   */
  isRange(date: NgbDate) {
    return date.equals(this.fromNGDate) || date.equals(this.toNGDate) || this.isInside(date) || this.isHovered(date);
  }

//NUEVO USUARIO
created(){

  // if (this.newUser.contrasenia.length < 7) {
  //   Swal.fire(
  //     'Información',
  //     'La clave necesita 6 caracteres',
  //     'warning'
  //   );
  //   return;
  // }
  
  // if (this.newUser.password.length < 7) {
  //   Swal.fire(
  //     'Información',
  //     'Contraseña minina 6 caracteres',
  //     'warning'
  //   );
  //   return;
  // }
  // if (this.newUser.rol.length === 0) {
  //   Swal.fire(
  //     'Información',
  //     'Hay campos que necesitan tu atención',
  //     'warning'
  //   );
  //   return;
  // }
 
  this.newUser.fecha = moment().format('YYYY-MM-DD');
  const randomId = Math.random().toString(36).substring(2, 8);
  const path = `Usuarios/${new Date().getTime()}_${randomId}`;
  const ref = this.storage.ref(path);
  ref.putString(this.newUser.cedula, 'data_url').then(() => {
    const url = ref.getDownloadURL();
    url.subscribe(src => {
      this.newUser.cedula = src;
      this.db.add('Usuarios', this.newUser).then(() => {
        Swal.fire(
          'Correcto',
          'Usuario registrado',
          'success'
        );
        this.clearForm();
       // this.spinner.show();
        this.ngOnInit();
        //this.cerrar();
        this.newUser = {
          nombre: '',
          correo:'',
          contrasenia:'',
          tipo:'',
          celular:'',
          fecha: moment().format('YYYY-MM-DD'),
          cedula: '',
          nombreEmpresa: '',
          anosExperiencia: '',
          recomendado: false,
          estrellas: '',
          id_user_action:this.authenticated.currentUser.id,
          verif_cedula:true,
          verif_celular:false,
          verif_correo:false,
          bloqueado:false,
          status:true,         
        };
      }).catch(error => {
        this.spinner.hide();
        Swal.fire(
          'Error',
          'Revisa tu conexion a internet',
          'error'
        );
      });
    });
  }).catch(error => {
    this.spinner.hide();
    Swal.fire(
      'Error',
      'Revisa tu conexion a internet',
      'error'
    );
  });
}

}
