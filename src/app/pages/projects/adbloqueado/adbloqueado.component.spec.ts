import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdbloqueadoComponent } from './adbloqueado.component';

describe('AdbloqueadoComponent', () => {
  let component: AdbloqueadoComponent;
  let fixture: ComponentFixture<AdbloqueadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdbloqueadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdbloqueadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
