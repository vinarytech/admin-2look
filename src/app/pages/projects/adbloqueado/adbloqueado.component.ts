import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import{ Observable} from 'rxjs/Observable';
import { DatabaseService } from '../../../services/database.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../project.model';
import { projectData } from '../projectdata';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-adbloqueado',
  templateUrl: './adbloqueado.component.html',
  styleUrls: ['./adbloqueado.component.scss']
})
export class AdbloqueadoComponent implements OnInit {

  public items: Observable<any[]>;
  // bread crumb items
 breadCrumbItems: Array<{}>;

 projectData: Project[];

  constructor(private db: DatabaseService,private modalService: NgbModal,private router: Router,private spinner: NgxSpinnerService){ }

  ngOnInit() {
   this.init();
   this.breadCrumbItems = [{ label: 'Administradores' }, { label: 'Activos', active: true }];
 
    //this.projectData = projectData;
   // this.db.col$('Administradores').subscribe(listDoc =>console.log("LISTA",listDoc));
  
  }
  init() {
  
   this.projectData = [];
   this.db.col$('Administradores').subscribe((data: any) => {
     const datos = data;
     for (const data of datos){
       if (data.status===false) {
         this.projectData.push(data);
         console.log("DATOS",data);
       }
     }
     //this.publicado = data;
    
 
     // this.projectData = data;
   }, error => {
    
 
   }).add (() => {
    //this.spinner.hide();
   });
 }
 
  Actual(){
    console.log("Funcion añador admin");
  }
  openModal(content: any) {
   this.modalService.open(content, { centered: true });
   //this.editdata=user;
 }
 activar(data){
  this.db.updated('Administradores/'+ data,{ status:true})
  .then(()=>{
    this.spinner.show();
      Swal.fire(
        'Correcto',
        'Administrador activo!',
        'success'
      )
    console.log("Actualizado")
    this.init();
    //this.router.navigate(['/projects/list']);
    this.spinner.hide();
  }  
  )
  .catch(err=>{
      Swal.fire({
        icon: 'error',
        title: 'ERROR',
        text: 'No se realizo la acción',
      });
      console.log("ERROR",err);
    });
  
  
  console.log("RESULTADO",data);
  }
 }
 