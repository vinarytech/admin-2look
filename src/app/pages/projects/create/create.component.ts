import { Component, OnInit, Input, EventEmitter, ViewChild, Output } from '@angular/core';
import * as moment from 'moment/moment';
// import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
// import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
// AÑADIR PARA AGREGAR USUARIO
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
// import { ApiService } from '../../../core/services/api.service';
import { Subcribe } from '../../../core/models/auth.models';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { DatabaseService } from '../../../services/database.service';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFireAuth } from 'angularfire2/auth';
// import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})

/**
 * Projects-create component
 */
export class CreateComponent implements OnInit {
 // UserForm: FormGroup;

//  if(FormGroup=false){
//   console.log();
// }



UserForm = new FormGroup({
  nombre: new FormControl('', [Validators.required, Validators.minLength(5)]),
  email: new FormControl('', [Validators.required, Validators.email]),
  password: new FormControl('', [Validators.required, Validators.minLength(5)]),
  rol: new FormControl('', [Validators.required]),
  fotografia: new FormControl('', [Validators.required]),
  fecha: new FormControl(moment().format('YYYY-MM-DD')),
  // id_user_action: new FormControl('', [Validators.required]),
  id_user_action: new FormControl('Developer'),
  status: new FormControl(true),
});

  constructor(private formBuilder: FormBuilder,
              private asFT: AngularFireStorage ,
              private db: DatabaseService,
              private afAuth: AngularFireAuth,
              private spinner: NgxSpinnerService,
              private router: Router) {

              }

  // bread crumb items
  breadCrumbItems: Array<{}>;

  // hoveredDate: NgbDate;
  // fromNGDate: NgbDate;
  // toNGDate: NgbDate;
  selected: any;
  hidden: boolean;
  fileData: File=null;
  previewUrlLicencia:any=null;
  fotografia:any;
  
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
  editar = false;
  licencia: any;
  currentUser: any;
  cardImageBase64: string;
  isImageSaved: boolean;
  imageError: string;
  uri: any;
  aux:any;
  previewUrlCedula: any = null;
  previewCedula: any = null;
  imgBase64Path: any;


  hoveredDate: NgbDate;
   fromNGDate: NgbDate;
  toNGDate: NgbDate;
  // selected: any;
  // hidden: boolean;

  @Input() fromDate: Date;
  @Input() toDate: Date;
  @Output() dateRangeSelected: EventEmitter<{}> = new EventEmitter();

  @ViewChild('dp', { static: true }) datePicker: any;

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Usuarios' }, { label: 'Nuevo', active: true }];
    this.selected = '';
    this.hidden = true;
  }

  // INPUT
  fileProgressLicencia(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;
      const allowed_types = ['image/png', 'image/jpeg'];
      const max_height = 15200;
      const max_width = 25600;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError =
          'Maximum size allowed is ' + max_size / 1000 + 'Mb';

        return false;
      }
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          const img_height = rs.currentTarget['height'];
          const img_width = rs.currentTarget['width'];
          this.imgBase64Path = e.target.result;
          this.cardImageBase64 = this.imgBase64Path;
          this.isImageSaved = true;
          this.UserForm.patchValue({fotografia: this.imgBase64Path});
          console.log('PATH');
          console.log(this.imgBase64Path);
          console.log('FORM');
          console.log(this.UserForm.value);
          this.aux = true;
          // this.previewImagePath = imgBase64Path;

        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
      this.fileData = <File>fileInput.target.files[0];
    }
  }

  previewLicencia() {
    // Show preview 
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = (_event) => {
      this.previewUrlLicencia = reader.result;
      console.log(this.previewLicencia);
      console.log(this.fileData);
      
      this.UserForm.patchValue({fotografia: this.fileData})
    }
  }

  ObtenerFoto(){
    const randomId = Math.random().toString(36).substring(2, 8);
      const path = `Administradores/${new Date().getTime()}_${randomId}`; // full path file
      const ref = this.asFT.ref(path);
      ref.putString(this.fotografia, 'data_url').then(() => {
        const url = ref.getDownloadURL();
        url.subscribe((url) => {
          console.log(url);
            this.UserForm.patchValue({fotografia: url})
        });
      }).catch(error => {
      // this.alerts.presentToast(error.message, 'danger');
      });
  }

  files: File[] = [];

  onSelect(event:any) {
    console.log(event);
    
    // if(this.files && this.files.length >=2) {
    //   this.onRemove(this.files[0]);
    // }
    // this.files.push(...event.addedFiles);
    
  }

  onRemove(event) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  // buildForm() {
  //   this.UserForm = this.formBuilder.group({
  //     nombre: ['', [Validators.required,  Validators.minLength(5)]],
  //     email: ['', [Validators.required, Validators.email]],
  //     password: ['', [Validators.required,  Validators.minLength(5)]],
  //     rol: ['', [Validators.required,  Validators]],
  //   });
  // }

  clearForm() {
    this.UserForm.patchValue({ nombre: '' });
    this.UserForm.patchValue({ email: '' });
    this.UserForm.patchValue({ password: '' });
  }

  onSubmit() {
    
    console.log(this.UserForm.value);
    // this.auth.auth.createUserWithEmailAndPassword(data.email, data.password).then(userData => {
    //   this.afDB.database.ref('usuarios/aplicacion/' + userData.user.uid).set(data);
    //   this.authService.isAuthenticated();
    //   this.modalController.dismiss({status: true});
    // }).catch(error => {
    //   console.log(error.message);
    //   this.alerts.presentToast(error.message, 'danger');
    // });
    // this.db.add('Administradores', this.UserForm.value);
    // this.router.navigate(['/projects/list']);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromNGDate = date;
      this.fromDate = new Date(date.year, date.month - 1, date.day);
      this.selected = '';
    } else if (this.fromDate && !this.toDate && date.after(this.fromNGDate)) {
      this.toNGDate = date;
      this.toDate = new Date(date.year, date.month - 1, date.day);
      this.hidden = true;
      this.selected = this.fromDate.toLocaleDateString() + '-' + this.toDate.toLocaleDateString();

      this.dateRangeSelected.emit({ fromDate: this.fromDate, toDate: this.toDate });

      this.fromDate = null;
      this.toDate = null;
      this.fromNGDate = null;
      this.toNGDate = null;

    } else {
      this.fromNGDate = date;
      this.fromDate = new Date(date.year, date.month - 1, date.day);
      this.selected = '';
    }
  }
  isHovered(date: NgbDate) {
    return this.fromNGDate && !this.toNGDate && this.hoveredDate && date.after(this.fromNGDate) && date.before(this.hoveredDate);
  }
  isInside(date: NgbDate) {
    return date.after(this.fromNGDate) && date.before(this.toNGDate);
  }
  isRange(date: NgbDate) {
    return date.equals(this.fromNGDate) || date.equals(this.toNGDate) || this.isInside(date) || this.isHovered(date);
  }
}
