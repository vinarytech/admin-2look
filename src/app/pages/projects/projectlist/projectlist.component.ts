import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { DatabaseService } from '../../../services/database.service';
import { Project } from '../project.model';
import { AngularFireStorage } from 'angularfire2/storage';
import * as moment from 'moment/moment';
import { AuthenticatedService } from '../../../core/services/authenticated.service';


@Component({
  selector: 'app-projectlist',
  templateUrl: './projectlist.component.html',
  styleUrls: ['./projectlist.component.scss']
})

/**
 * Projects-list component
 */
export class ProjectlistComponent implements OnInit {

 // bread crumb items
 breadCrumbItems: Array<{}>;
 activos = true;
 projectData: Project[];
 activosData = [];
 inactivosData = [];
 publicado: Project[];
 editdata: any;
 newUser = {
   email: '',
   fecha: moment().format('YYYY-MM-DD'),
   fotografia: '',
   id_user_action: this.authenticated.currentUser.id,
   nombre: '',
   password: '',
   rol: '',
   status: true
 };
 constructor(private authenticated: AuthenticatedService,
             private modalService: NgbModal,
             private db: DatabaseService,
             private router: Router,
             private storage: AngularFireStorage,
             private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.init();
    this.breadCrumbItems = [{ label: 'Administradores' }, { label: 'Activos', active: true }];
  }
  changedData(){
    if (this.activos) {
      this.projectData = this.inactivosData;
      this.activos = false;
    } else {
      this.projectData = this.activosData;
      this.activos = true;
    }
  }
  init() {
    this.spinner.show();
    this.db.col$('Administradores').subscribe((datos: any) => {
      this.projectData = [];
      this.activosData = [];
      this.inactivosData = [];
      this.activos = true;
      for (const data of datos){
        if (data.status) {
          this.activosData.push(data);
        } else {
          this.inactivosData.push(data);
        }
      }
      this.projectData = this.activosData;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire(
        'Error',
        'Revisa tu conexión a internet',
        'error'
      );
    });
  }
  actualizar(){
    this.spinner.show();
    if (this.editdata.changed_fotografia) {
      const randomId = Math.random().toString(36).substring(2, 8);
      const path = `productos/${new Date().getTime()}_${randomId}`;
      const ref = this.storage.ref(path);
      ref.putString(this.editdata.fotografia, 'data_url').then(() => {
        const url = ref.getDownloadURL();
        url.subscribe(src => {
          const data = {
            nombre: this.editdata.nombre,
            fotografia: src,
            email: this.editdata.email,
            password: this.editdata.password,
            rol: this.editdata.rol,
            id_user_action:this.authenticated.currentUser.id,
            fecha_modifica:moment().format('YYYY-MM-DD'),
          };
          this.db.updated('Administradores/' + this.editdata.id, data).then(() => {
            this.init();
            Swal.fire(
              'Correcto',
              'Administrador actualizado!',
              'success'
            );
            this.cerrar();
            this.spinner.hide();
          }).catch(err => {
            this.spinner.hide();
            Swal.fire({
              icon: 'error',
              title: 'ERROR',
              text: 'Revisa tu conexión a internet',
            });
          });
        });
      }).catch(error => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'Revisa tu conexión a internet',
        });
      });
    } else {
      const data = {
        nombre: this.editdata.nombre,
        email: this.editdata.email,
        password: this.editdata.password,
        rol: this.editdata.rol,
        id_user_action:this.authenticated.currentUser.id,
        fecha_modifica:moment().format('YYYY-MM-DD'),
      };
      console.log(this.editdata.id);
      this.db.updated('Administradores/' + this.editdata.id, data).then(() => {
        this.init();
        Swal.fire(
          'Correcto',
          'Administrador actualizado!',
          'success'
        );
        this.cerrar();
        this.spinner.hide();
      }).catch(err => {
        console.log(err);
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'Revisa tu conexión a internet',
        });
      });
    }
  }
  cerrar() {
    this.modalService.dismissAll();
    this.closeModal();
  }
  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    localStorage.setItem('content', JSON.stringify(user));
    this.editdata = JSON.parse(localStorage.getItem('content'));
    this.editdata.changed_fotografia = false;
  }
  openModalNuevo(content: any) {
    this.modalService.open(content, { centered: true });
  }
  closeModal(){
    localStorage.removeItem('content');
  }
  fileProgress(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          this.newUser.fotografia = e.target.result;
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }
  fileProgressLicencia(fileInput: any) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        image.onload = rs => {
          this.editdata.fotografia = e.target.result;
          this.editdata.changed_fotografia = true;
        };
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }
  desactivar(data){
    this.spinner.show();
    this.db.updated('Administradores/' + data, { status: false, id_user_action:this.authenticated.currentUser.id,fecha_modifica:moment().format('YYYY-MM-DD')}).then(() => {
      this.spinner.hide();
      this.init();
    }).catch(err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'ERROR',
        text: 'Revisa tu conexion a internet',
      });
    });
  }
  activar(data){
    this.spinner.show();
    this.db.updated('Administradores/' + data, { status: true,id_user_action:this.authenticated.currentUser.id,fecha_modifica:moment().format('YYYY-MM-DD')}).then(() => {
      this.spinner.hide();
      this.init();
    }).catch(err => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'ERROR',
        text: 'Revisa tu conexion a internet',
      });
    });
  }
  created(){
    if (this.newUser.email.length === 0) {
      Swal.fire(
        'Información',
        'Hay campos que necesitan tu atención',
        'warning'
      );
      return;
    }
    if (this.newUser.fotografia.length === 0) {
      Swal.fire(
        'Información',
        'Hay campos que necesitan tu atención',
        'warning'
      );
      return;
    }
    if (this.newUser.nombre.length === 0) {
      Swal.fire(
        'Información',
        'Hay campos que necesitan tu atención',
        'warning'
      );
      return;
    }
    if (this.newUser.password.length < 7) {
      Swal.fire(
        'Información',
        'Contraseña minina 6 caracteres',
        'warning'
      );
      return;
    }
    if (this.newUser.rol.length === 0) {
      Swal.fire(
        'Información',
        'Hay campos que necesitan tu atención',
        'warning'
      );
      return;
    }
    this.spinner.show();
    this.newUser.fecha = moment().format('YYYY-MM-DD');
    const randomId = Math.random().toString(36).substring(2, 8);
    const path = `administradores/${new Date().getTime()}_${randomId}`;
    const ref = this.storage.ref(path);
    ref.putString(this.newUser.fotografia, 'data_url').then(() => {
      const url = ref.getDownloadURL();
      url.subscribe(src => {
        this.newUser.fotografia = src;
        this.db.add('Administradores', this.newUser).then(() => {
          Swal.fire(
            'Correcto',
            'Administrador registrado',
            'success'
          );
          this.init();
          this.cerrar();
          this.newUser = {
            email: '',
            fecha: moment().format('YYYY-MM-DD'),
            fotografia: '',
            id_user_action:this.authenticated.currentUser.id,
            nombre: '',
            password: '',
            rol: '',
            status: true
          };
        }).catch(error => {
          this.spinner.hide();
          Swal.fire(
            'Error',
            'Revisa tu conexion a internet',
            'error'
          );
        });
      });
    }).catch(error => {
      this.spinner.hide();
      Swal.fire(
        'Error',
        'Revisa tu conexion a internet',
        'error'
      );
    });
  }
}
