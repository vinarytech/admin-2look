import { Component, EventEmitter, OnInit } from '@angular/core';
import { Project } from './reporteuser.model';
//import { ApiService } from '../../../core/services/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
@Component({
  selector: 'app-reporteuser',
  templateUrl: './reporteuser.component.html',
  styleUrls: ['./reporteuser.component.scss']
})
export class ReporteuserComponent implements OnInit {

  source: LocalDataSource;
  data = [
    {
      id: 4,
      name: 'Patricia Lebsack',
      email: 'Julianne.OConner@kory.org',
      passed: 'Yes',
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      passed: 'No',
    },
    {
      id: 11,
      name: 'Nicholas DuBuque',
      email: 'Rey.Padberg@rosamond.biz',
      passed: 'Yes',
    },
  ];

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'NOMBRES',
  //       // editable: false,
  //       // addable: false,
  //     },


  //     name: {
  //       title: 'APELLIDOS',
  //     },
  //     username: {
  //       title: 'EMAIL',
  //     },
  //     email: {
  //       title: 'PAIS',
  //     },
  //   },
  // };
  settings = {
    //VER TODOS LOS ELEMENTOS

    pager: { display: false },
    columns: {
      
      // id: {
      //   title: 'ID',
      // },
      nombre: {
        title: 'NOMBRE',
      },
      // correo: {
      //   title: 'CORREO',
      // },
      celular: {
        title: 'CELULAR',
      },
      estrellas: {
        title: 'ESTRELLAS',
      },
      tipo: {
        title: 'TIPO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'usuario', title: 'Usuario' },
              { value: 'profesional', title: 'Profesional' },
            ],
          },
        },
      },
      // name: {
      //   title: 'Full Name',
      //   filter: {
      //     type: 'list',
      //     config: {
      //       selectText: 'Select...',
      //       list: [
      //         { value: 'Glenna Reichert', title: 'Glenna Reichert' },
      //         { value: 'Kurtis Weissnat', title: 'Kurtis Weissnat' },
      //         { value: 'Chelsey Dietrich', title: 'Chelsey Dietrich' },
      //       ],
      //     },
      //   },
      // },


      verif_celular: {
        title: 'Ver. Tlfno.',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      },
      
      verif_correo: {
        title: 'Ver. Correo',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      },
      verif_cedula: {
        title: 'Ver. Cédula',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      },
    
     
      bloqueado: {
        title: 'Ver. Bloq.',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      }, 
      recomendado: {
        title: 'Recomen.',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      },  
      anosExperiencia: {
        title: 'Experien.',
      },
      fecha: {
        title: 'Creación',
      },
      fecha_modifica: {
        title: 'Modificación',
      }
      // ,
      // id_user_action: {
      //   title: 'ADMIN',
      // },
      
     
    },
  };
  
  breadCrumbItems: Array<{}>;

  projectData: Project[];
 editdata: any;


  constructor(
    public authenticated: AuthenticatedService,
    private db: DatabaseService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router) {
    this.source = new LocalDataSource();
    
    this.db.col$('Usuarios').subscribe((data: any) => {
    // this.api.report_user_list().subscribe((data:any) => {
      const todos = []
      let aux = {
        // id: 10,
        nombre:'',
        correo:'',
        pais:'',
        celular: '',
        estrellas: '',
        anosExperiencia:'',
        verif_celular:true,
        verif_correo:true,
        verif_cedula:true,
        bloqueado:1,
        super_whiser:1,
        super_thanker:1,
        fecha:'',
        fecha_modifica:'',
        id_user_action:''
      
        
        // passed: 'No',
      }
      for(const x of data) {
        // aux.id = x.id;
       // aux.name = x.nombre + x.apellidos;
       aux.nombre = x.nombre;
       aux.correo = x.correo;
        aux.celular = x.celular;
        aux.estrellas = x.estrellas;
        aux.anosExperiencia = x.anosExperiencia;
        aux.verif_cedula = x.verif_cedula;
        aux.verif_correo = x.verif_correo;
        aux.verif_cedula = x.verif_cedula;
        aux.bloqueado = x.bloqueado;
        aux.super_whiser = x.super_whiser;
        aux.super_thanker = x.super_thanker;
        aux.fecha = x.fecha;
        aux.fecha_modifica = x.fecha_modifica;
       
        todos.push(x);
         // console.log("X ES IGUAL",x);
      }
      // console.log("TODOS",todos);
      this.source.load(todos);
    
    
    });
    

   // this.source = new LocalDataSource(this.data);
   }
 
 
  ngOnInit() {
    this.init();
  //  this.spinner.show();
    this.breadCrumbItems = [{ label: 'Reporte' }, { label: 'Reporte Usuarios', active: true }];
  
   }
  
   init() {
    this.spinner.show();
 
    this.projectData = [];
    this.db.col$('Usuarios').subscribe((data: any) => {
      const datos = data;
      for (const data of datos){
        // if (data.status===true) {
        //   this.projectData.push(data);
        //   console.log("DATOS",data);
        // }
        this.projectData.push(data);
        // console.log("DATOS",data);
        this.spinner.hide();
      }
      //this.publicado = data;
     
  
      // this.projectData = data;
    }, error => {
     
  
    }).add (() => {
     //this.spinner.hide();
    });
  }
   onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      // {
      //   field: 'id',
      //   search: query
      // },
      {
        field: 'nombre',
        search: query
      },
      {
        field: 'telefono',
        search: query
      },
      {
        field: 'pais',
        search: query
      },
      {
        field: 'username',
        search: query
      },
      {
        field: 'email',
        search: query
      }
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }
   //

  
 
  

  
  }
  