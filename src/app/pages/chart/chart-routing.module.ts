import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApexComponent } from './apex/apex.component';
import { ChartjsComponent } from './chartjs/chartjs.component';
import { ChartistComponent } from './chartist/chartist.component';
import { EchartComponent } from './echart/echart.component';
import { ReporteingresosComponent } from './reporteingresos/reporteingresos.component';
import { ReporteuserComponent } from './reporteuser/reporteuser.component';
import { ReporteservicioComponent } from './reporteservicio/reporteservicio.component';
import { ReportesolicitudComponent } from './reportesolicitud/reportesolicitud.component';
import { ReportemultasComponent } from './reportemultas/reportemultas.component';
const routes: Routes = [
    {
        path: 'apex',
        component: ApexComponent
    },
    {
        path: 'chartjs',
        component: ChartjsComponent
    },
    {
        path: 'chartist',
        component: ChartistComponent
    },
    {
        path: 'echart',
        component: EchartComponent
    },
    {
        path: 'reporteingresos',
        component: ReporteingresosComponent
    },
    {
        path: 'reporteuser',
        component: ReporteuserComponent
    },
    {
        path: 'reporteservicio',
        component: ReporteservicioComponent
    },
    {
        path: 'reportesolicitud',
        component: ReportesolicitudComponent
    },
    {
        path: 'reportemultas',
        component: ReportemultasComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ChartRoutingModule { }
