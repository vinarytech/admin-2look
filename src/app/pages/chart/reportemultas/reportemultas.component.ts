import { Component, EventEmitter, OnInit } from '@angular/core';
import { Project } from './reporteingresos.model';
//import { ApiService } from '../../../core/services/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
@Component({
  selector: 'app-reportemultas',
  templateUrl: './reportemultas.component.html',
  styleUrls: ['./reportemultas.component.scss']
})
export class ReportemultasComponent implements OnInit {

  source: LocalDataSource;
  data = [
    {
      id: 4,
      name: 'Patricia Lebsack',
      email: 'Julianne.OConner@kory.org',
      passed: 'Yes',
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      passed: 'No',
    },
    {
      id: 11,
      name: 'Nicholas DuBuque',
      email: 'Rey.Padberg@rosamond.biz',
      passed: 'Yes',
    },
  ];

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'NOMBRES',
  //       // editable: false,
  //       // addable: false,
  //     },


  //     name: {
  //       title: 'APELLIDOS',
  //     },
  //     username: {
  //       title: 'EMAIL',
  //     },
  //     email: {
  //       title: 'PAIS',
  //     },
  //   },
  // };
  settings = {
    //VER TODOS LOS ELEMENTOS

    pager: { display: false },
    columns: {
      // id: {
      //   title: 'ID',
      // },
      forma_pago: {
        title: 'FORMA PAGO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'Transferencia', title: 'Transferencia' },
              { value: 'Tarjeta', title: 'Tarjeta' },
            ],
          },
        },
      },
      
      
      id_transferencia: {
        title: 'ID TRANSFERENCIA',
      },
     
      
      // name: {
      //   title: 'Full Name',
      //   filter: {
      //     type: 'list',
      //     config: {
      //       selectText: 'Select...',
      //       list: [
      //         { value: 'Glenna Reichert', title: 'Glenna Reichert' },
      //         { value: 'Kurtis Weissnat', title: 'Kurtis Weissnat' },
      //         { value: 'Chelsey Dietrich', title: 'Chelsey Dietrich' },
      //       ],
      //     },
      //   },
      // },
      
      total: {
        title: 'TOTAL',
      },
    
    
      pago: {
        title: 'ESTADO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'PENDIENTE', title: 'PENDIENTE' },
              { value: 'VERIFICADO', title: 'VERIFICADO' },
              { value: 'RECHAZADO', title: 'RECHAZADO' },
            ],
          },
        },
      },
      fecha: {
        title: 'CREACIÓN',
      },
      fecha_cancela: {
        title: 'MODIFICACIÓN',
      },
     
    },
  };
  
  breadCrumbItems: Array<{}>;

  projectData: Project[];
 editdata: any;
 activosData = [];
 inactivosData = [];
 activos = true;

  constructor(public authenticated: AuthenticatedService,private db: DatabaseService,private modalService: NgbModal, private spinner: NgxSpinnerService,private router: Router) {
    this.source = new LocalDataSource();
    this.db.col$('Pago_multas').subscribe((data: any) => {
    // this.api.report_user_list().subscribe((data:any) => {
      const todos = []
      let aux = {
        // id: 10,
        forma_pago:'',
        id_transferencia:'',
        total:'',
        tipo: '',
        fecha: '',
        fecha_cancela:'',
        pago:'',
        
      
        
        // passed: 'No',
      }
      for(const x of data) {
        // aux.id = x.id;
       // aux.name = x.nombre + x.apellidos;
       aux.forma_pago = x.forma_pago;
       aux.id_transferencia = x.id_transferencia;
        aux.total = x.total;
        aux.fecha = x.fecha;
        aux.fecha_cancela = x._cancela;
        aux.pago = x.pago;
        //aux.total = x.total;
      
        todos.push(x);
         // console.log("X ES IGUAL",x);
      }
      // console.log("TODOS",todos);
      this.source.load(todos);
     // console.log("TODOS",todos);
    
    });
    

   // this.source = new LocalDataSource(this.data);
   }
 
 
  ngOnInit() {
    this.init();
  //  this.spinner.show();
    this.breadCrumbItems = [{ label: 'Reporte' }, { label: 'Reporte Ingresos Multas', active: true }];
  
   }
  
   init() {
 
    this.projectData = [];
    this.db.col$('Pago_multas').subscribe((data: any) => {
      const datos = data;
      for (const data of datos){
        // if (data.status===true) {
        //   this.projectData.push(data);
        //   console.log("DATOS",data);
        // }
      
          this.projectData.push(data);
          // console.log("DATOS",data);
        
      }
      //this.publicado = data;
     
  
      // this.projectData = data;
    }, error => {
     
  
    }).add (() => {
     //this.spinner.hide();
    });
    this.spinner.show();
    this.db.col$('Administradores').subscribe((datos: any) => {
      this.projectData = [];
      this.activosData = [];
      this.inactivosData = [];
      this.activos = true;
      for (const data of datos){
        if (data.status) {
          this.activosData.push(data);
        } else {
          this.inactivosData.push(data);
        }
      }
      this.projectData = this.activosData;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      Swal.fire(
        'Error',
        'Revisa tu conexión a internet',
        'error'
      );
    });

  }
   onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      // {
      //   field: 'id',
      //   search: query
      // },
      {
        field: 'nombre',
        search: query
      },
      {
        field: 'telefono',
        search: query
      },
      {
        field: 'pais',
        search: query
      },
      {
        field: 'username',
        search: query
      },
      {
        field: 'email',
        search: query
      }
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }
   //

  
 
  

  
  }
  