import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportemultasComponent } from './reportemultas.component';

describe('ReportemultasComponent', () => {
  let component: ReportemultasComponent;
  let fixture: ComponentFixture<ReportemultasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportemultasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportemultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
