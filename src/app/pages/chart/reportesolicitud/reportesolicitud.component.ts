import { Component, EventEmitter, OnInit } from '@angular/core';
import { Project } from './reportesolicitud.model';
//import { ApiService } from '../../../core/services/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
@Component({
  selector: 'app-reportesolicitud',
  templateUrl: './reportesolicitud.component.html',
  styleUrls: ['./reportesolicitud.component.scss']
})
export class ReportesolicitudComponent implements OnInit {

  source: LocalDataSource;
  data = [
    {
      id: 4,
      name: 'Patricia Lebsack',
      email: 'Julianne.OConner@kory.org',
      passed: 'Yes',
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      passed: 'No',
    },
    {
      id: 11,
      name: 'Nicholas DuBuque',
      email: 'Rey.Padberg@rosamond.biz',
      passed: 'Yes',
    },
  ];

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'NOMBRES',
  //       // editable: false,
  //       // addable: false,
  //     },


  //     name: {
  //       title: 'APELLIDOS',
  //     },
  //     username: {
  //       title: 'EMAIL',
  //     },
  //     email: {
  //       title: 'PAIS',
  //     },
  //   },
  // };
  settings = {
    //VER TODOS LOS ELEMENTOS

    pager: { display: false },
    columns: {
      // id: {
      //   title: 'ID',
      // },
      direccion: {
        title: 'CIUDAD',
      },

     
      tipo: {
        title: 'TIPO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'domicilio', title: 'Domicilio' },
              { value: 'establecimiento', title: 'Establecimiento' },
            ],
          },
        },
      },
      subcategoria: {
        title: 'CATEGORIA',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'producto', title: 'Producto' },
              { value: 'servicio', title: 'Servicio' },
            ],
          },
        },
      },
      // name: {
      //   title: 'Full Name',
      //   filter: {
      //     type: 'list',
      //     config: {
      //       selectText: 'Select...',
      //       list: [
      //         { value: 'Glenna Reichert', title: 'Glenna Reichert' },
      //         { value: 'Kurtis Weissnat', title: 'Kurtis Weissnat' },
      //         { value: 'Chelsey Dietrich', title: 'Chelsey Dietrich' },
      //       ],
      //     },
      //   },
      // },

      total: {
        title: 'TOTAL',
      },
      estado: {
        title: 'ESTADO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'PENDIENTE', title: 'Pendiente' },
              { value: 'POR PAGAR', title: 'Por pagar' },
              { value: 'EN TRANSCURSO', title: 'En transcurso' },
              { value: 'FINALIZADO', title: 'Finalizado' },
            ],
          },
        },
      },
      fecha: {
        title: 'FECHA',
      }
      //, fechaaceptada: {
      //   title: 'FECHA ACEPTADA',
      // },


    },
  };

  breadCrumbItems: Array<{}>;

  projectData: Project[];
  editdata: any;

  statData = [{
    icon: 'bx bx-star',
    title: 'Recomendados',
    value: 0
  },

  {
    icon: 'bx bx-user-check',
    title: 'Profesionales',
    value: 0
  }];
  monto: any;
  constructor(public authenticated: AuthenticatedService, private db: DatabaseService, private modalService: NgbModal, private spinner: NgxSpinnerService, private router: Router) {
    this.source = new LocalDataSource();
    this.db.col$('Solicitudes').subscribe((data: any) => {
      let total = 0;
      let monto = 0;


      // this.api.report_user_list().subscribe((data:any) => {
      const todos = []
      let aux = {
        // id: 10,
        direccion: '',
        estado: '',
        fecha: '',
        fechaaceptada:'',
        tipo: '',
        subcategoria: '',
        total: '',



        // passed: 'No',
      }
      for (const x of data) {
        // aux.id = x.id;
        // aux.name = x.nombre + x.apellidos;
        aux.direccion = x.direccion;
        aux.estado = x.estado;
        aux.fecha = x.fecha;
        aux.fechaaceptada = x.fechaaceptada;
        aux.tipo = x.tipo;
        aux.subcategoria = x.subcategoria;
        aux.total = x.total;


        todos.push(x);

        // console.log("X ES IGUAL",x);
      }
      // console.log("TODOS",todos);
      this.source.load(todos);



      // for (const solicitud of data) {
      //   if (solicitud.estado === "FINALIZADO") {
      //     monto = monto + solicitud.total;
      //     this.monto = data.monto;
      //   }

      // }
      // console.log("MONTO", monto)





    });


    // this.source = new LocalDataSource(this.data);
  }



  ngOnInit() {
    this.init();
    //  this.spinner.show();
    this.breadCrumbItems = [{ label: 'Reporte' }, { label: 'Reporte Ingresos APP', active: true }];

  }

  init() {

    this.spinner.show();
    this.projectData = [];
    this.db.col$('Solicitudes').subscribe((data: any) => {
      const datos = data;
      for (const data of datos) {
        // if (data.status===true) {
        //   this.projectData.push(data);
        //   console.log("DATOS",data);
        // }

        this.projectData.push(data);
        this.spinner.hide();


      }
      //this.publicado = data;


      // this.projectData = data;
    }, error => {


    }).add(() => {
      //this.spinner.hide();
    });
  }
  onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      // {
      //   field: 'id',
      //   search: query
      // },
      {
        field: 'nombre',
        search: query
      },
      {
        field: 'telefono',
        search: query
      },
      {
        field: 'pais',
        search: query
      },
      {
        field: 'username',
        search: query
      },
      {
        field: 'email',
        search: query
      }
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }
  //






}
