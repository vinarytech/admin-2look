import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportesolicitudComponent } from './reportesolicitud.component';

describe('ReportesolicitudComponent', () => {
  let component: ReportesolicitudComponent;
  let fixture: ComponentFixture<ReportesolicitudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportesolicitudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportesolicitudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
