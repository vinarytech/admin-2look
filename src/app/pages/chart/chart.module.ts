import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgApexchartsModule } from 'ng-apexcharts';
import { ChartsModule } from 'ng2-charts';
import { NgxChartistModule } from 'ngx-chartist';
import { NgxEchartsModule } from 'ngx-echarts';

import { UIModule } from '../../shared/ui/ui.module';
import { ChartRoutingModule } from './chart-routing.module';

import { ApexComponent } from './apex/apex.component';
import { ChartjsComponent } from './chartjs/chartjs.component';
import { ChartistComponent } from './chartist/chartist.component';
import { EchartComponent } from './echart/echart.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ReporteingresosComponent } from './reporteingresos/reporteingresos.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ReporteuserComponent } from './reporteuser/reporteuser.component';
import { ReporteservicioComponent } from './reporteservicio/reporteservicio.component';
import { ReportesolicitudComponent } from './reportesolicitud/reportesolicitud.component';
import { ReportemultasComponent } from './reportemultas/reportemultas.component';

@NgModule({
  declarations: [ApexComponent, ChartjsComponent, ChartistComponent, EchartComponent, ReporteingresosComponent, ReporteuserComponent, ReporteservicioComponent, ReportesolicitudComponent, ReportemultasComponent],
  imports: [
    CommonModule,
    ChartRoutingModule,
    UIModule,
    NgApexchartsModule,
    ChartsModule,
    NgxChartistModule,
    NgxEchartsModule,
    NgxSpinnerModule,
    Ng2SmartTableModule
  ]
})
export class ChartModule { }
