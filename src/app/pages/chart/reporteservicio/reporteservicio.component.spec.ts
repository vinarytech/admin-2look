import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteservicioComponent } from './reporteservicio.component';

describe('ReporteservicioComponent', () => {
  let component: ReporteservicioComponent;
  let fixture: ComponentFixture<ReporteservicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteservicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteservicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
