import { Component, EventEmitter, OnInit } from '@angular/core';
import { Project } from './reporteservicio.model';
//import { ApiService } from '../../../core/services/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
@Component({
  selector: 'app-reporteservicio',
  templateUrl: './reporteservicio.component.html',
  styleUrls: ['./reporteservicio.component.scss']
})
export class ReporteservicioComponent implements OnInit {
  source: LocalDataSource;
  data = [
    {
      id: 4,
      name: 'Patricia Lebsack',
      email: 'Julianne.OConner@kory.org',
      passed: 'Yes',
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      passed: 'No',
    },
    {
      id: 11,
      name: 'Nicholas DuBuque',
      email: 'Rey.Padberg@rosamond.biz',
      passed: 'Yes',
    },
  ];

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'NOMBRES',
  //       // editable: false,
  //       // addable: false,
  //     },


  //     name: {
  //       title: 'APELLIDOS',
  //     },
  //     username: {
  //       title: 'EMAIL',
  //     },
  //     email: {
  //       title: 'PAIS',
  //     },
  //   },
  // };
  settings = {
    //VER TODOS LOS ELEMENTOS

    pager: { display: false },
    columns: {
      // id: {
      //   title: 'ID',
      // },
      ciudad: {
        title: 'CIUDAD',
      },
      sectores: {
        title: 'SECTORES',
      },
      subcategoria: {
        title: 'TIPO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'producto', title: 'Producto' },
              { value: 'servicio', title: 'Servicio' }          
            ],
          },
        },
      },
      categoria: {
        title: 'CATEGORÍA',
      },
      descripcion: {
        title: 'DESCRIPCIÓN',
      },
      genero: {
        title: 'ORIENTADO A: ',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'Masculino', title: 'Hombres' },
              { value: 'Femenino', title: 'Mujeres' },
              { value: 'Masculino / Femenino', title: 'Unisex' }             
            ],
          },
        },
      },
      cancelacion: {
        title: 'TIPO CANCELACIÓN',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'Sin Cancelación', title: 'Sin Cancelación' },
              { value: 'Flexible (Hasta 8 horas)', title: 'Flexible (Hasta 8 horas)' },
              { value: 'Moderado', title: 'Moderado' }             
            ],
          },
        },
      },
      precio: {
        title: 'PRECIO',
      },
     
      destacado: {
        title: 'Destacado',
        filter: {
          type: 'checkbox',
          config: {
            true: 'true',
            false: 'false',
            resetText: 'Borrar',
          },
        },
      },
     
     
    },
  };
  
  breadCrumbItems: Array<{}>;

  projectData: Project[];
 editdata: any;


  constructor(
    public authenticated: AuthenticatedService,
    private db: DatabaseService,
    private modalService: NgbModal, 
    private spinner: NgxSpinnerService,
    private router: Router) {
    this.source = new LocalDataSource();
    this.db.col$('Servicios').subscribe((data: any) => {
    // this.api.report_user_list().subscribe((data:any) => {
      const todos = []
      let aux = {
        // id: 10,
        ciudad:'',
        sectores:'',
        subcategoria:'',
        categoria:'',
        descripcion:'',
        genero: '',
        cancelacion: '',
        precio: '',
        destacado:true,
        
      
        
        // passed: 'No',
      }
      for(const x of data) {
        // aux.id = x.id;
       // aux.name = x.nombre + x.apellidos;
       aux.ciudad = x.ciudad;
       aux.sectores = x.sectores;
       aux.subcategoria = x.subcategoria;
       aux.categoria = x.categoria;
        aux.descripcion = x.descripcion;
        aux.genero = x.genero;
        aux.cancelacion = x.cancelacion;
        aux.precio = x.precio;
        aux.destacado = x.destacado;
      
        todos.push(x);
         // console.log("X ES IGUAL",x);
      }
      // console.log("TODOS",todos);
      this.source.load(todos);
    
    
    });
    

   // this.source = new LocalDataSource(this.data);
   }
 
 
  ngOnInit() {
    this.init();
  //  this.spinner.show();
    this.breadCrumbItems = [{ label: 'Reporte' }, { label: 'Reporte Servicios', active: true }];
  
   }
  
   init() {
    this.spinner.show();
 
    this.projectData = [];
    this.db.col$('Servicios').subscribe((data: any) => {
      const datos = data;
      for (const data of datos){
        // if (data.status===true) {
        //   this.projectData.push(data);
        //   console.log("DATOS",data);
        // }
      
          this.projectData.push(data);
          // console.log("DATOS",data);
          this.spinner.hide();
        
      }
      //this.publicado = data;
     
  
      // this.projectData = data;
    }, error => {
     
  
    }).add (() => {
     //this.spinner.hide();
    });
  }
   onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      // {
      //   field: 'id',
      //   search: query
      // },
      {
        field: 'nombre',
        search: query
      },
      {
        field: 'telefono',
        search: query
      },
      {
        field: 'pais',
        search: query
      },
      {
        field: 'username',
        search: query
      },
      {
        field: 'email',
        search: query
      }
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }
   //

  
 
  

  
  }
  