import { Component, EventEmitter, OnInit } from '@angular/core';
import { Project } from './reporteingresos.model';
//import { ApiService } from '../../../core/services/api.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { DatabaseService } from '../../../services/database.service';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
//import { SesionService } from 'src/app/core/services/sesion.service';
@Component({
  selector: 'app-reporteingresos',
  templateUrl: './reporteingresos.component.html',
  styleUrls: ['./reporteingresos.component.scss']
})
export class ReporteingresosComponent implements OnInit {

  source: LocalDataSource;
  data = [
    {
      id: 4,
      name: 'Patricia Lebsack',
      email: 'Julianne.OConner@kory.org',
      passed: 'Yes',
    },
    {
      id: 5,
      name: 'Chelsey Dietrich',
      email: 'Lucio_Hettinger@annie.ca',
      passed: 'No',
    },
    {
      id: 11,
      name: 'Nicholas DuBuque',
      email: 'Rey.Padberg@rosamond.biz',
      passed: 'Yes',
    },
  ];

  // settings = {
  //   columns: {
  //     id: {
  //       title: 'NOMBRES',
  //       // editable: false,
  //       // addable: false,
  //     },


  //     name: {
  //       title: 'APELLIDOS',
  //     },
  //     username: {
  //       title: 'EMAIL',
  //     },
  //     email: {
  //       title: 'PAIS',
  //     },
  //   },
  // };
  settings = {
    //VER TODOS LOS ELEMENTOS

    pager: { display: false },
    columns: {
      // id: {
      //   title: 'ID',
      // },
      forma_pago: {
        title: 'FORMA PAGO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'Transferencia', title: 'Transferencia' },
              { value: 'Tarjeta', title: 'Tarjeta' },
            ],
          },
        },
      },
      
      
      id_transferencia: {
        title: 'ID TRANSFERENCIA',
      },
     
      
      // name: {
      //   title: 'Full Name',
      //   filter: {
      //     type: 'list',
      //     config: {
      //       selectText: 'Select...',
      //       list: [
      //         { value: 'Glenna Reichert', title: 'Glenna Reichert' },
      //         { value: 'Kurtis Weissnat', title: 'Kurtis Weissnat' },
      //         { value: 'Chelsey Dietrich', title: 'Chelsey Dietrich' },
      //       ],
      //     },
      //   },
      // },
      
      total: {
        title: 'TOTAL',
      },
     
      pago: {
        title: 'ESTADO',
        filter: {
          type: 'list',
          config: {
            selectText: 'Select...',
            list: [
              { value: 'PENDIENTE', title: 'PENDIENTE' },
              { value: 'VERIFICADO', title: 'VERIFICADO' },
              { value: 'RECHAZADO', title: 'RECHAZADO' },
            ],
          },
        },
      },
      fecha: {
        title: 'Creación',
      },
      
      fecha_modifica: {
        title: 'Modificación',
      },
    
     
    },
  };
  
  breadCrumbItems: Array<{}>;

  projectData: Project[];
 editdata: any;


  constructor(public authenticated: AuthenticatedService,private db: DatabaseService,private modalService: NgbModal, private spinner: NgxSpinnerService,private router: Router) {
    this.source = new LocalDataSource();
    this.db.col$('Pago_paquete').subscribe((data: any) => {
    // this.api.report_user_list().subscribe((data:any) => {
      const todos = []
      let aux = {
        // id: 10,
        forma_pago:'',
        id_transferencia:'',
        total:'',
        tipo: '',
        fecha: '',
        pago:'',
        fecha_modifica:''
        
      
        
        // passed: 'No',
      }
      for(const x of data) {
        // aux.id = x.id;
       // aux.name = x.nombre + x.apellidos;
       aux.forma_pago = x.forma_pago;
       aux.id_transferencia = x.id_transferencia;
        aux.total = x.total;
        aux.fecha = x.fecha;
        aux.pago = x.pago;
        aux.fecha_modifica = x.fecha_modifica;
        //aux.total = x.total;
      
        todos.push(x);
         // console.log("X ES IGUAL",x);
      }
      // console.log("TODOS",todos);
      this.source.load(todos);
     
    
    });
    

   // this.source = new LocalDataSource(this.data);
   }
 
 
  ngOnInit() {
    this.init();
  //  this.spinner.show();
    this.breadCrumbItems = [{ label: 'Reporte' }, { label: 'Reporte Ingresos Packs', active: true }];
  
   }
  
   init() {
    this.spinner.show();
 
    this.projectData = [];
    this.db.col$('Pago_paquete').subscribe((data: any) => {
      const datos = data;
      for (const data of datos){
        // if (data.status===true) {
        //   this.projectData.push(data);
        //   console.log("DATOS",data);
        // }
      
          this.projectData.push(data);
          // console.log("DATOS",data);
          this.spinner.hide();
        
      }
      //this.publicado = data;
     
  
      // this.projectData = data;
    }, error => {
     
  
    }).add (() => {
     //this.spinner.hide();
    });
   // this.spinner.show();
  }
   onSearch(query: string = '') {
    this.source.setFilter([
      // fields we want to include in the search
      // {
      //   field: 'id',
      //   search: query
      // },
      {
        field: 'nombre',
        search: query
      },
      {
        field: 'telefono',
        search: query
      },
      {
        field: 'pais',
        search: query
      },
      {
        field: 'username',
        search: query
      },
      {
        field: 'email',
        search: query
      }
    ], false);
    // second parameter specifying whether to perform 'AND' or 'OR' search 
    // (meaning all columns should contain search query or at least one)
    // 'AND' by default, so changing to 'OR' by setting false here
  }
   //

  
 
  

  
  }
  