import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminbloqComponent } from './adminbloq.component';

describe('AdminbloqComponent', () => {
  let component: AdminbloqComponent;
  let fixture: ComponentFixture<AdminbloqComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminbloqComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminbloqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
