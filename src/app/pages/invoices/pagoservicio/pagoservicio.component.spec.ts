import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagoservicioComponent } from './pagoservicio.component';

describe('PagoservicioComponent', () => {
  let component: PagoservicioComponent;
  let fixture: ComponentFixture<PagoservicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagoservicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagoservicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
