import { Component, OnInit } from '@angular/core';

import { Usergrid } from './usergrid.model';

import { userGridData } from './data';
import { DatabaseService } from '../../../services/database.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../../email/problemas/problemas.model';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { analytics } from 'firebase';

@Component({
  selector: 'app-pagoservicio',
  templateUrl: './pagoservicio.component.html',
  styleUrls: ['./pagoservicio.component.scss']
})
export class PagoservicioComponent implements OnInit {
  breadCrumbItems: Array<{}>;

  userGridData: Usergrid[];
  projectData: Project[];
  servicio: Project[];
  producto: Project[];
  usuarios: Project[];
  usuarior: Project[];
  usuarios2: Project[];
  usuarior2: Project[];

  id_profe: any;
  id_usuario: any;
  editdata: any;
  encargado: any;
  encarga: any;

  constructor(private db: DatabaseService, private modalService: NgbModal, private spinner: NgxSpinnerService, private router: Router) {
    this.encargado = "";
  }

  ngOnInit() {
  
    this.init();
    this.breadCrumbItems = [{ label: 'Transacciones' }, { label: 'Pago servicios / productos ', active: true }];

    /**
     * fetches data
     */
    this._fetchData();
  }

  init() {
   
    //   this.projectData = [];

    //  this.db.col$('Solicitudes').subscribe((data: any) => {
    //    const datos = data;

    //  for (const data of datos){
    //    if (data.estado === "POR PAGAR") {
    //      this.projectData.push(data);
    //      //console.log("DATOS",data);
    //      const prof=data.id_profesional;
    //      console.log("PROFESIONAL ID",prof);
    //      this.id_profe=data.id_profesional;
    //    }
    //  }


    this.db.col$('Solicitudes').subscribe((data: any) => {
      this.encargado = "";
      this.projectData = [];
      this.servicio = [];
      this.producto = [];
      const datos = data;
      // for (const data of datos){
      //   if (data.status===true) {
      //     this.projectData.push(data);
      //     console.log("DATOS",data);
      //   }
      // }
      for (const data of datos) {
        if (data.estado === "POR PAGAR" && data.subcategoria === "servicio") {




          this.servicio.push(data);
          console.log("SERVICIO RESULT", data);

          this.usuarios = [];
          this.usuarior = [];

          this.db.col$('Usuarios').subscribe((data2: any) => {
            const datos2 = data2;
            for (const data2 of datos2) {

              if (data.idencargado === data2.id) {
                this.usuarios.push(data2);
                
              } if (data.idsolicitante === data2.id) {
                this.usuarior.push(data2);
                
              }
              
            }
          }, error => {

           
          }).add(() => {
           
          });




        } if (data.estado === "POR PAGAR" && data.subcategoria === "producto") {
          this.producto.push(data);
          console.log("PRODUCTO RESULT", data);


          this.usuarios2 = [];
          this.usuarior2 = [];

          this.db.col$('Usuarios').subscribe((data2: any) => {
            const datos2 = data2;
            for (const data2 of datos2) {

              if (data.idencargado === data2.id) {
                this.usuarios2.push(data2);
                console.log("SERVICIO RESUT", this.usuarios);

              } if (data.idsolicitante === data2.id) {
                this.usuarior2.push(data2);
              }
            }
          }, error => {


          }).add(() => {
            this.spinner.hide();
          });
        }
      }
      // console.log(data);
    }, error => {


    }).add(() => {
      //this.spinner.hide();
    });


  }
  /**
   * User grid data fetches
   */
  private _fetchData() {
    this.userGridData = userGridData;
  }

  aprobar(data) {
    this.db.updated('Pago_paquete/' + data, { pago: "Verificado" })

      .then(() => {
        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Transferencia verificada!',
          'success'
        )
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        this.init();
        this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });


    console.log("RESULTADO PROFESIONAL", this.id_profe);


  }


  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    this.editdata = user;
  }
}
