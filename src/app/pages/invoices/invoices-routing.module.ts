import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { PagoComponent } from './pago/pago.component';
import { PagoservicioComponent } from './pagoservicio/pagoservicio.component';
import { PagomultaComponent } from './pagomulta/pagomulta.component';
const routes: Routes = [
    {
        path: 'list',
        component: ListComponent
    },
    {
        path: 'detail',
        component: DetailComponent
    },
    {
        path: 'pago',
        component: PagoComponent
    },
    {
        path: 'pagoservicio',
        component: PagoservicioComponent
    }
    ,
    {
        path: 'pagomulta',
        component: PagomultaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InvoicesRoutingModule {}
