import { Component, OnInit } from '@angular/core';

import { Usergrid } from './usergrid.model';

import { userGridData } from './data';
import { DatabaseService } from '../../../services/database.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../../email/problemas/problemas.model';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { analytics } from 'firebase';
import { collectExternalReferences } from '@angular/compiler';
import { toJSDate } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';
import { timestamp } from 'rxjs/operators';
import * as moment from 'moment/moment';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.scss']
})
export class PagoComponent implements OnInit {
  breadCrumbItems: Array<{}>;

  userGridData: Usergrid[];
  projectData: Project[];
  profesionales: Project[];
  usuarios: Project[];
  id_profe: any;
  id_usuario: any;
  editdata: any;
  hoy: any;
  total: any;
  fecha: string;
  newUser = {
    accion: 'Verificaciones',
    date: moment().format('YYYY-MM-DD'),
    idreceptor: '',
    idemisor: this.authenticated.currentUser.id,
    notificacion: ''
    // nombre: '',
    // password: '',
    // rol: '',
    // status: true
  };
  constructor(
    private authenticated: AuthenticatedService,
    private db: DatabaseService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router) {
    var fecha = new Date();
    fecha.toString();
    // console.log("HOY", fecha);

  }

  ngOnInit() {
    this.init();
    this.breadCrumbItems = [{ label: 'Transacciones' }, { label: 'Pago paquetes', active: true }];



    // const fecha=new Date();
    // fecha.setDate;



    // this.projectData = [];

    // this.db.col$('Mensajes').subscribe((data: any) => {


    //       this.projectData.push(data);
    //       console.log(data.fecha);
    //      const current =new Date();
    //      current.setDate = data[0].fecha;
    //      console.log("CURRENT",current);




    //   });

    // this.userGridData = data;
    // console.log(data);




    /**
     * fetches data
     */
    this._fetchData();
  }

  init() {

    this.spinner.show();

    this.db.col$('Pago_paquete').subscribe((data: any) => {
      this.projectData = [];
      const datos = data;
      // console.log(datos);
      for (const data of datos) {
        if (data.forma_pago === "Transferencia" && data.pago === "Pendiente" && data.status === true) {
          this.projectData.push(data);
          //console.log("DATOS",data);
          //    const hoy=new Date();
          //  console.log("HOY",hoy);+
          const pagar = data.total;
          this.total = data.total;

        }

        this.spinner.hide();
      }


      // console.log("TOTAL ES :", this.total);
      // this.userGridData = data;
      // console.log(data);
    }, error => {


    }).add(() => {
      //this.spinner.hide();
    });
  }
  /**
   * User grid data fetches
   */
  private _fetchData() {
    this.userGridData = userGridData;
  }
  activar(data, token, user) {


    if (this.total === 5) {
      // console.log("ES EL PACK BASICO");

      //FALTA FECHA
      this.db.updated('Paquetes/' + data, { id_user_action: this.authenticated.currentUser.id, paquete1: true, estado: true, estadosoli: false })

        .then(() => {
          this.db.sendNotification('Activado', 'Su paquete ha sido activado', token).subscribe((response: any) => {
            // GUARDAR DATOS
            this.newUser.date = moment().format('YYYY-MM-DD');
            this.newUser = {
              accion: 'Paquete',
              date: moment().format('YYYY-MM-DD HH:mm'),
              idreceptor: user,
              idemisor: this.authenticated.currentUser.id,
              notificacion: 'El profesional ha activado el paquete básico $5'
              // nombre: '',
              // password: '',
              // rol: '',
              // status: true

            };
            this.db.add('Notificaciones', this.newUser).then(() => {
              // Swal.fire(
              //   'Correcto',
              //   'Administrador registrado',
              //   'success'
              // );
              // this.init();
              // this.cerrar();

              console.log("receptor", data.id);

            }).catch(error => {
              this.spinner.hide();
              Swal.fire(
                'Error',
                'Revisa tu conexion a internet',
                'error'
              );
            });

          }, error => {
            console.error(error);
          });




          // this.spinner.show();
          Swal.fire(
            'Correcto',
            'Paquete activado!',
            'success'
          )
        }
        )
        .catch(err => {
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'No se realizo la acción',
          });
          console.log("ERROR", err);
        });

    } else if (this.total === 15) {
      // console.log("ES EL PACK NORMAL");

      //FALTA FECHA
      this.db.updated('Paquetes/' + data, { paquete1: false, id_user_action: this.authenticated.currentUser.id, paquete2: moment(Date.now() + 31 * 24 * 3600 * 1000).format('DD/MM/YYYY'), estado: true, estadosoli: false })
        .then(() => {
          this.db.sendNotification('Activado', 'Su paquete ha sido activado', token).subscribe((response: any) => {
            // GUARDAR DATOS
            this.newUser.date = moment().format('YYYY-MM-DD');
            this.newUser = {
              accion: 'Paquete',
              date: moment().format('YYYY-MM-DD HH:mm'),
              idreceptor: user,
              idemisor: this.authenticated.currentUser.id,
              notificacion: 'El profesional ha activado el paquete normal $15'
              // nombre: '',
              // password: '',
              // rol: '',
              // status: true

            };
            this.db.add('Notificaciones', this.newUser).then(() => {
              // Swal.fire(
              //   'Correcto',
              //   'Administrador registrado',
              //   'success'
              // );
              // this.init();
              // this.cerrar();

              console.log("receptor", data.id);

            }).catch(error => {
              this.spinner.hide();
              Swal.fire(
                'Error',
                'Revisa tu conexion a internet',
                'error'
              );
            });

          }, error => {
            console.error(error);
          });





          // this.spinner.show();
          Swal.fire(
            'Correcto',
            'Paquete activado!',
            'success'
          )
        }
        )
        .catch(err => {
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'No se realizo la acción',
          });
          console.log("ERROR", err);
        });


    } else if (this.total === 25) {
      // console.log("ES EL PACK DESTACADO");
      //FALTA FECHA
      this.db.updated('Paquetes/' + data, { paquete1: false, id_user_action: this.authenticated.currentUser.id, paquete3: moment(Date.now() + 31 * 24 * 3600 * 1000).format('DD/MM/YYYY'), estado: true, estadosoli: false })

        .then(() => {
          this.db.sendNotification('Activado', 'Su paquete ha sido activado', token).subscribe((response: any) => {
            // GUARDAR DATOS
            this.newUser.date = moment().format('YYYY-MM-DD');
            this.newUser = {
              accion: 'Paquete',
              date: moment().format('YYYY-MM-DD HH:mm'),
              idreceptor: user,
              idemisor: this.authenticated.currentUser.id,
              notificacion: 'El profesional ha activado el paquete descatado $25'
              // nombre: '',
              // password: '',
              // rol: '',
              // status: true

            };
            this.db.add('Notificaciones', this.newUser).then(() => {
              // Swal.fire(
              //   'Correcto',
              //   'Administrador registrado',
              //   'success'
              // );
              // this.init();
              // this.cerrar();

              console.log("receptor", data.id);

            }).catch(error => {
              this.spinner.hide();
              Swal.fire(
                'Error',
                'Revisa tu conexion a internet',
                'error'
              );
            });

          }, error => {
            console.error(error);
          });




          // this.spinner.show();
          Swal.fire(
            'Correcto',
            'Paquete activado!',
            'success'
          )
        }
        )
        .catch(err => {
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'No se realizo la acción',
          });
          console.log("ERROR", err);
        });




    }



  }

  aprobar(data, id_transferencia) {


    this.db.updated('Pago_paquete/' + data, { pago: "Verificado", id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD'), id_transferencia: id_transferencia })

      .then(() => {



        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Transferencia verificada!',
          'success'
        )
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        this.init();
        this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });




  }



  Rechazar(data, negado) {
    this.db.updated('Pago_paquete/' + data, { id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD'), negado: negado, pago: "Rechazado" })
      .then(() => {
        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Verificación rechazada!',
          'success'
        )
        this.cerrar();
        console.log("Actualizado")
        this.init();
        // this.router.navigate(['/projects/adbloqueado']);
        this.spinner.hide();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });


    console.log("RESULTADO", data);
  }
  cerrar() {
    this.modalService.dismissAll();
  }
  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    this.editdata = user;
  }



}
