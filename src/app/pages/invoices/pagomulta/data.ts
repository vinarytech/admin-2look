const userGridData = [
    {
        id: 1,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
        text: 'PASAPORTE',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    },
    {
        id: 2,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
         text: 'CEDULA',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    },
    {
        id: 3,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
        text: 'PASAPORTE',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    },
    {
        id: 4,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
        text: 'PASAPORTE',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    },
    {
        id: 5,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
        text: 'PASAPORTE',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    },
    {
        id: 6,
        image: 'assets/images/users/avatar-5.jpg',
         name: 'David McHenry',
        text: 'PASAPORTE',
        projects: ['APROBADO', 'DENEGADO'],
        email: 'david@skote.com'
    }
];

export { userGridData };
