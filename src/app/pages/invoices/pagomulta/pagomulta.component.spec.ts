import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagomultaComponent } from './pagomulta.component';

describe('PagomultaComponent', () => {
  let component: PagomultaComponent;
  let fixture: ComponentFixture<PagomultaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagomultaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagomultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
