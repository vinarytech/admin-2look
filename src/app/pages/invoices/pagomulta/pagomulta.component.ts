import { Component, OnInit } from '@angular/core';

import { Usergrid } from './usergrid.model';

import { userGridData } from './data';
import { DatabaseService } from '../../../services/database.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../../email/problemas/problemas.model';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { analytics } from 'firebase';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
import * as moment from 'moment/moment';
@Component({
  selector: 'app-pagomulta',
  templateUrl: './pagomulta.component.html',
  styleUrls: ['./pagomulta.component.scss']
})
export class PagomultaComponent implements OnInit {
  breadCrumbItems: Array<{}>;
  editdata: any;
  userGridData: Usergrid[];
  projectData: Project[];
  profesionales: Project[];
  usuarios: Project[];
  id_profe: any;
  id_usuario: any;

  newUser = {
    accion: 'Verificaciones',
    date: moment().format('YYYY-MM-DD'),
    idreceptor: '',
    idemisor: this.authenticated.currentUser.id,
    notificacion: ''
    // nombre: '',
    // password: '',
    // rol: '',
    // status: true
  };
  constructor(private authenticated: AuthenticatedService, private db: DatabaseService, private modalService: NgbModal, private spinner: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.init();
    this.breadCrumbItems = [{ label: 'Transacciones' }, { label: 'Pago multas', active: true }];

    /**
     * fetches data
     */
    this._fetchData();
  }

  init() {

    this.spinner.show();

    this.db.col$('Pago_multas').subscribe((data: any) => {
      this.projectData = [];
      const datos = data;
      // console.log(datos);
      for (const data of datos) {
        if (data.forma_pago === "Transferencia" && data.pago === "Pendiente" && data.status === true) {


          this.projectData.push(data);

          //  this.db.col$('Usuarios').subscribe((data2: any) => {
          //     const datos2 = data2;
          //     for (const data2 of datos2) {

          //       if (data.id_usuario === data2.id) {
          //         //this.usuarios.push(data2);
          //         console.log("SON IGUALES",data);
          //       } 


          //     }
          //   }, error => {


          //   }).add(() => {
          //     this.spinner.hide();
          //   });


        }
        this.spinner.hide();
      }

      // this.userGridData = data;
      // console.log(data);
    }, error => {


    }).add(() => {
      //this.spinner.hide();
    });


  }
  /**
   * User grid data fetches
   */
  private _fetchData() {
    this.userGridData = userGridData;
  }

  aprobar(data, id_transferencia, token, user) {

    this.db.updated('Pago_multas/' + data, { pago: "Verificado", id_transferencia: id_transferencia, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })

      .then(() => {

        this.db.sendNotification('Verificación pago', 'Su pago por transferencia ha sido verificado correctamente', token).subscribe((response: any) => {
          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Verificación',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: user,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Se ha verificado el pago de la multa por transferencia'
            // nombre: '',
            // password: '',
            // rol: '',
            // status: true

          };
          this.db.add('Notificaciones', this.newUser).then(() => {
            // Swal.fire(
            //   'Correcto',
            //   'Administrador registrado',
            //   'success'
            // );
            // this.init();
            this.cerrar();

            console.log("receptor", data.id);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });








        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Transferencia verificada!',
          'success'
        )
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        this.init();
        this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });


    //console.log("RESULTADO PROFESIONAL", this.id_profe);


  }

  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    this.editdata = user;
  }
  Rechazar(data, negado,token,user) {
    this.db.updated('Pago_multas/' + data, { negado: negado, pago: "Rechazado", id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD'),status:false})
    
    .then(() => {

      this.db.sendNotification('Verificación pago', 'Su pago por transferencia ha sido negado, revise sus notificaciones', token).subscribe((response: any) => {
        // GUARDAR DATOS
        this.newUser.date = moment().format('YYYY-MM-DD');
        this.newUser = {
          accion: 'Verificación',
          date: moment().format('YYYY-MM-DD HH:mm'),
          idreceptor: user,
          idemisor: this.authenticated.currentUser.id,
          notificacion: 'Se ha negado el pago de la multa por transferencia - Razón: ' + negado
          // nombre: '',
          // password: '',
          // rol: '',
          // status: true

        };
        this.db.add('Notificaciones', this.newUser).then(() => {
          // Swal.fire(
          //   'Correcto',
          //   'Administrador registrado',
          //   'success'
          // );
          // this.init();
          this.cerrar();

          console.log("receptor", data);

        }).catch(error => {
          this.spinner.hide();
          Swal.fire(
            'Error',
            'Revisa tu conexion a internet',
            'error'
          );
        });

      }, error => {
        console.error(error);
      });




        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Verificación rechazada!',
          'success'
        )
        this.cerrar();
        console.log("Actualizado")
        this.init();
        // this.router.navigate(['/projects/adbloqueado']);
        this.spinner.hide();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });


    // console.log("RESULTADO", data);
  }
  cerrar() {
    this.modalService.dismissAll();
  }
}
