import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { InvoicesRoutingModule } from './invoices-routing.module';
import { UIModule } from '../../shared/ui/ui.module';

import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PagoComponent } from './pago/pago.component';
import { PagoservicioComponent } from './pagoservicio/pagoservicio.component';
import { PagomultaComponent } from './pagomulta/pagomulta.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListComponent, DetailComponent, PagoComponent, PagoservicioComponent, PagomultaComponent],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    UIModule,
    NgbTooltipModule,
    NgxSpinnerModule,
    FormsModule,
    NgbModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class InvoicesModule { }
