import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

import { listData } from './data';

import { InvoiceList } from './list.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

/**
 * Invoices list component
 */
export class ListComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;

  listData: InvoiceList[];

  constructor(private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Transacciones' }, { label: 'Pagos', active: true }];

    /**
     * fetches the data
     */
    this._fetchData();
  }

  /**
   * fetches the invoice list data
   */
  private _fetchData() {
    this.listData = listData;
  }
}
