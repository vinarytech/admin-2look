import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfrecomendadoComponent } from './profrecomendado.component';

describe('ProfrecomendadoComponent', () => {
  let component: ProfrecomendadoComponent;
  let fixture: ComponentFixture<ProfrecomendadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfrecomendadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfrecomendadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
