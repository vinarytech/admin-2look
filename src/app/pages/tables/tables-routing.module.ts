import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BasicComponent } from './basic/basic.component';
import { AdvancedtableComponent } from './advancedtable/advancedtable.component';
import { UsbloqueadoComponent } from './usbloqueado/usbloqueado.component';
import { ProfnuevoComponent } from './profnuevo/profnuevo.component';
import { ProfrecomendadoComponent } from './profrecomendado/profrecomendado.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { NuevoComponent } from './nuevo/nuevo.component';
const routes: Routes = [
    {
        path: 'basic',
        component: BasicComponent
    },
    {
        path: 'advanced',
        component: AdvancedtableComponent
    }
    ,
    {
        path: 'usbloqueado',
        component: UsbloqueadoComponent
    },
    {
        path: 'profnuevo',
        component: ProfnuevoComponent
    }
    ,
    {
        path: 'profrecomendado',
        component: ProfrecomendadoComponent
    }
    ,
    {
        path: 'usuarios',
        component: UsuariosComponent
    }
    ,
    {
        path: 'nuevo',
        component: NuevoComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TablesRoutingModule { }
