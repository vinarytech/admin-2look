import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfnuevoComponent } from './profnuevo.component';

describe('ProfnuevoComponent', () => {
  let component: ProfnuevoComponent;
  let fixture: ComponentFixture<ProfnuevoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfnuevoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfnuevoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
