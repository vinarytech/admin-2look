import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UIModule } from '../../shared/ui/ui.module';
import { NgbPaginationModule, NgbTypeaheadModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { TablesRoutingModule } from './tables-routing.module';
import { AdvancedSortableDirective } from './advancedtable/advanced-sortable.directive'
import { BasicComponent } from './basic/basic.component';
import { AdvancedtableComponent } from './advancedtable/advancedtable.component';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap';
import { UsbloqueadoComponent } from './usbloqueado/usbloqueado.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ProfnuevoComponent } from './profnuevo/profnuevo.component';
import { ProfrecomendadoComponent } from './profrecomendado/profrecomendado.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { NuevoComponent } from './nuevo/nuevo.component';
@NgModule({
  declarations: [BasicComponent, AdvancedtableComponent, AdvancedSortableDirective, UsbloqueadoComponent, ProfnuevoComponent, ProfrecomendadoComponent, UsuariosComponent, NuevoComponent],
  imports: [
    CommonModule,
    TablesRoutingModule,
    UIModule,
    NgbPaginationModule,
    NgbTypeaheadModule,
    NgbCollapseModule,
    NgbDropdownModule,
    FormsModule,
    NgxSpinnerModule
  ]
})
export class TablesModule { }
