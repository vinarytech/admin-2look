import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsbloqueadoComponent } from './usbloqueado.component';

describe('UsbloqueadoComponent', () => {
  let component: UsbloqueadoComponent;
  let fixture: ComponentFixture<UsbloqueadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsbloqueadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsbloqueadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
