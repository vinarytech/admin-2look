import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { DecimalPipe } from '@angular/common';

import { Observable } from 'rxjs';

import { Table } from './advanced.model';

import { tableData } from './data';

import { AdvancedService } from './advanced.service';
import { AdvancedSortableDirective, SortEvent } from './advanced-sortable.directive';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { DatabaseService } from '../../../services/database.service';
import { Project } from '../../projects/project.model';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import * as moment from 'moment/moment';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
@Component({
  selector: 'app-usbloqueado',
  templateUrl: './usbloqueado.component.html',
  styleUrls: ['./usbloqueado.component.scss'],
  providers: [AdvancedService, DecimalPipe]
})
export class UsbloqueadoComponent implements OnInit {
 // bread crum data
 breadCrumbItems: Array<{}>;
 // Table data
 tableData: Table[];
 public selected: any;
 hideme: boolean[] = [];
 tables$: Observable<Table[]>;
 total$: Observable<number>;

 @ViewChildren(AdvancedSortableDirective) headers: QueryList<AdvancedSortableDirective>;
 public isCollapsed = true;
 editdata: any;
 datos: any;
 
 projectData2: Project[];
 newUser = {
  accion: 'Verificaciones',
  date: moment().format('YYYY-MM-DD'),
  idreceptor: '',
  idemisor: this.authenticated.currentUser.id,
  notificacion:''
  // nombre: '',
  // password: '',
  // rol: '',
  // status: true
};
  constructor(
    private authenticated: AuthenticatedService,
    private router: Router,
    public service: AdvancedService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private db: DatabaseService) 
  { 
    this.tables$ = service.tables$;
    this.total$ = service.total$;
  }

  ngOnInit() {
    this.init();
    this.breadCrumbItems = [{ label: 'Aplicación' }, { label: 'Bloqueados', active: true }];
    /**
     * fetch data
     */
    this._fetchData();
    
  
  }
  init() {
    
    this.spinner.show();
  this.db.col$('Usuarios').subscribe((data: any) => {
    this.projectData2 = [];
    const datos = data;
    for (const data of datos){
      if (data.bloqueado===true) {
        this.projectData2.push(data);
        // console.log("DATOS",data);
        
      }
      this.spinner.hide();
    }
      //  console.log(data);
      //  this.datos = data;


    }, error => {
 
    }).add (() => {
     
    });
  }

  changeValue(i) {
    this.hideme[i] = !this.hideme[i];
  }

Actual(){
  console.log("Funcion añador user");
}
  /**
   * fetches the table value
   */
  _fetchData() {
    this.tableData = tableData;
    for (let i = 0; i <= this.tableData.length; i++) {
      this.hideme.push(true);
    }
  }

  /**
   * Sort table data
   * @param param0 sort the column
   *
   */
  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }
  openModal(content: any,user) {
    this.modalService.open(content, { centered: true });
    this.editdata=user;
  }
  openModal2(content: any) {
    this.modalService.open(content, { centered: true });
   // this.editdata=user;
  }
  recomendar(data){
    this.db.updated('Usuarios/'+ data,{ recomendado:true})
    .then(()=>{
      this.spinner.show();
        Swal.fire(
          'Correcto',
          'Profesional recomendado!',
          'success'
        )
      console.log("Actualizado")
      this.init();
      // this.router.navigate(['/tables/profrecomendado']);
      this.spinner.hide();
    }  
    )
    .catch(err=>{
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR",err);
      });
    
    
    console.log("RESULTADO",data);
    }

    desbloquear(data){
      this.db.updated('Usuarios/'+ data.id,{ bloqueado:false,id_user_action:this.authenticated.currentUser.id,fecha_modifica:moment().format('YYYY-MM-DD')})
      
        .then(() => {
          this.db.sendNotification('Desbloqueo', 'Usted ha sido desbloqueado', data.token).subscribe((response: any) => {
            // GUARDAR DATOS
            this.newUser.date = moment().format('YYYY-MM-DD');
            this.newUser = {
              accion: 'Bloqueo',
              date: moment().format('YYYY-MM-DD HH:mm'),
              idreceptor: data.id,
              idemisor:this.authenticated.currentUser.id,
              notificacion:'Usuario desbloqueado'
              // nombre: '',
              // password: '',
              // rol: '',
              // status: true
              
            };
            this.db.add('Notificaciones', this.newUser).then(() => {
              // Swal.fire(
              //   'Correcto',
              //   'Administrador registrado',
              //   'success'
              // );
              // this.init();
              // this.cerrar();
             
              console.log("receptor",data.id);
            
            }).catch(error => {
              this.spinner.hide();
              Swal.fire(
                'Error',
                'Revisa tu conexion a internet',
                'error'
              );
            });
      
          }, error => {
            console.error(error);
          });




        this.spinner.show();
          Swal.fire(
            'Correcto',
            'Desbloqueado!',
            'success'
          )
        console.log("Actualizado")
        this.init();
        // this.router.navigate(['/tables/profrecomendado']);
        this.spinner.hide();
      }  
      )
      .catch(err=>{
          Swal.fire({
            icon: 'error',
            title: 'ERROR',
            text: 'No se realizo la acción',
          });
          console.log("ERROR",err);
        });
      
      
      console.log("RESULTADO",data);
      }

  
  
}
