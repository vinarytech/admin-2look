import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})

/**
 * Ecommerce orders component
 */
export class OrdersComponent implements OnInit {

  // bread crumb items
  breadCrumbItems: Array<{}>;
  term: any;
  editdata: any;
  transactions;
  UserForm = new FormGroup({
    nombre: new FormControl('', [Validators.required, Validators.minLength(5)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(5)]),
    rol: new FormControl('', [Validators.required]),
    fotografia: new FormControl('', [Validators.required]),
    fecha: new FormControl('',[Validators.required]),
    fecha_modifica: new FormControl('', [Validators.required]),
    id_user_action: new FormControl('', [Validators.required]),
    status:new FormControl(true),
  });
  constructor(private spinner: NgxSpinnerService,private modalService: NgbModal) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Configuración' }, { label: 'Paquetes', active: true }];

    this.transactions = [
      {
        id: '#SK2548',
        name: 'Clark Benson',
        date: '01 Oct, 2019',
        total: '$345',
        status: 'Refund',
        payment: ['fab fa-cc-visa', 'Visa'],
        index: 9,
      },
    ];
  }
  openModal(content: any) {
    this.modalService.open(content, { centered: true });
    
    //this.editdata;
  }
  Actual(){
    console.log("Funcion añador PAQU");
  }
  onSubmit(){}
}
