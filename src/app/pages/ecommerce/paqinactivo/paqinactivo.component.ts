import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Project } from '../../../pages/email/problemas/problemas.model';
import { DatabaseService } from '../../../services/database.service';

@Component({
  selector: 'app-paqinactivo',
  templateUrl: './paqinactivo.component.html',
  styleUrls: ['./paqinactivo.component.scss']
})
export class PaqinactivoComponent implements OnInit {
  projectData: Project[];
  breadCrumbItems: Array<{}>;
  editdata: any;
  constructor(private spinner: NgxSpinnerService,private modalService: NgbModal,private router: Router,private db: DatabaseService) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'Configuración' }, { label: 'Paquetes', active: true }];
    this.init();
  }
  init() {
    //this.spinner.show();
    
    this.db.col$('Admin_paquetes').subscribe((data: any) => {
      this.projectData = [];
      const datos = data;
      for (const data of datos){
        if (data.status===false) {
          this.projectData.push(data);
        }
      }
    
    }, error => {
     
  
    }).add (() => {
     //this.spinner.hide();
    });

    // this.db.col$('Admin_paquetes').subscribe((data: any) => {
    //    console.log(data); 
       
       

   
       
   
  }

  /**
   * Open modal
   * @param content modal content
   */
  openModal(content: any,user) {
    this.modalService.open(content, { centered: true });
    this.editdata=user;
    console.log(this.editdata);
  }
  activar(data){
    this.db.updated('Admin_paquetes/'+ data,{ status:true})
    .then(()=>{
      this.spinner.show();
        Swal.fire(
          'Correcto',
          'Paquete activo!',
          'success'
        )
      console.log("Actualizado")
     this.init();
      // this.router.navigate(['/projects/adbloqueado']);
      this.spinner.hide();
    }  
    )
    .catch(err=>{
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR",err);
      });
    
    
    console.log("RESULTADO",data);
    }
 
}
