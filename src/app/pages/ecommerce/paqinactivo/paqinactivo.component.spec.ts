import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaqinactivoComponent } from './paqinactivo.component';

describe('PaqinactivoComponent', () => {
  let component: PaqinactivoComponent;
  let fixture: ComponentFixture<PaqinactivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaqinactivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaqinactivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
