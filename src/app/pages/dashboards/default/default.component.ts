import { Component, OnInit } from '@angular/core';
import { emailSentBarChart, monthlyEarningChart, transactions, statData } from './data';
import { ChartType } from './dashboard.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
import { DatabaseService } from '../../../services/database.service';
import Swal from 'sweetalert2';
import * as moment from 'moment/moment';
import { MessagingService } from '../../../../service/messaging.service';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss']
})
export class DefaultComponent implements OnInit {
  fecha = moment().format('YYYY');
  show = false;
  emailSentBarChart: ChartType = {
    chart: {
        height: 359,
        type: 'bar',
        stacked: true,
        toolbar: {
            show: false
        },
        zoom: {
            enabled: true
        }
    },
    plotOptions: {
        bar: {
            horizontal: false,
            columnWidth: '15%',
            endingShape: 'rounded'
        },
    },
    dataLabels: {
        enabled: false
    },
    series: [{
        name: 'Tarjeta de crédito',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }, {
        name: 'Transferencia',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }],
    xaxis: {
        categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
    },
    colors: ['#556ee6', '#f1b44c'],
    legend: {
        position: 'bottom',
    },
    fill: {
        opacity: 1
    },
  };
  statData = [{
    icon: 'bx bx-star',
    title: 'Recomendados',
    value: 0
  }, 
  
   {
    icon: 'bx bx-user-check',
    title: 'Profesionales',
    value: 0
  },
   {
    icon: 'bx bx-user',
    title: 'Usuarios',
    value: 0
  },
  {
    icon: 'bx bx-block',
    title: 'Bloqueados',
    value: 0
  },
  {
    icon: 'bx bx-dollar-circle',
    title: 'Pago paquetes',
    value: 0
  }, {
    icon: 'bx bx-dollar-circle',
    title: 'Pago multas',
    value: 0
  },
   {
    icon: 'bx bx-money',
    title: 'Ventas totales',
    value: 0
  },
  {
    icon: 'bx bx-x',
    title: 'Problemas',
    value: 0
  }

];
message;
  constructor(private spinner: NgxSpinnerService,
              private db: DatabaseService,
              public authenticated: AuthenticatedService,
              private messagingService: MessagingService) { }

  ngOnInit() {
    this.init();
    this.messagingService.requestPermission()
    this.messagingService.receiveMessage()
    this.message = this.messagingService.currentMessage;
    // console.log("PUSH",this.message);
  }

  restart() {
    this.emailSentBarChart.series[0].data[0] = 0;
    this.emailSentBarChart.series[0].data[1] = 0;
    this.emailSentBarChart.series[0].data[2] = 0;
    this.emailSentBarChart.series[0].data[3] = 0;
    this.emailSentBarChart.series[0].data[4] = 0;
    this.emailSentBarChart.series[0].data[5] = 0;
    this.emailSentBarChart.series[0].data[6] = 0;
    this.emailSentBarChart.series[0].data[7] = 0;
    this.emailSentBarChart.series[0].data[8] = 0;
    this.emailSentBarChart.series[0].data[9] = 0;
    this.emailSentBarChart.series[0].data[10] = 0;
    this.emailSentBarChart.series[0].data[11] = 0;
    this.emailSentBarChart.series[1].data[0] = 0;
    this.emailSentBarChart.series[1].data[1] = 0;
    this.emailSentBarChart.series[1].data[2] = 0;
    this.emailSentBarChart.series[1].data[3] = 0;
    this.emailSentBarChart.series[1].data[4] = 0;
    this.emailSentBarChart.series[1].data[5] = 0;
    this.emailSentBarChart.series[1].data[6] = 0;
    this.emailSentBarChart.series[1].data[7] = 0;
    this.emailSentBarChart.series[1].data[8] = 0;
    this.emailSentBarChart.series[1].data[9] = 0;
    this.emailSentBarChart.series[1].data[10] = 0;
    this.emailSentBarChart.series[1].data[11] = 0;
    this.statData[0].value = 0;
    this.statData[1].value = 0;
    this.statData[2].value = 0;
    this.statData[3].value = 0;
    this.statData[4].value = 0;
    this.statData[5].value = 0;
  }
  init(){
    this.spinner.show();

    

    this.db.col$('Usuarios').subscribe((usuarios: any) => {
      this.restart();
      this.show = false;
   //   this.statData[1].value = usuarios.length;
     
      //console.log(this.statData);
      for (const usuario of usuarios) {
        if (usuario.recomendado && usuario.tipo === 'profesional') {
          this.statData[0].value = +this.statData[0].value + 1;
        }
        if (usuario.recomendado===false && usuario.tipo === 'profesional' && usuario.bloqueado===false) {
          this.statData[1].value = +this.statData[1].value + 1;
        }
        if (usuario.tipo === 'usuario' && usuario.bloqueado===false) {
          this.statData[2].value = +this.statData[2].value + 1;
        }
        if (usuario.bloqueado ===true) {
          this.statData[3].value = +this.statData[3].value + 1;
        }
      }
      let total = 0;
      this.db.col$('Pago_paquete').subscribe((paquetes: any) => {
        let monto = 0;
        for (const paquete of paquetes) {
          if (paquete.status&&paquete.pago==="Verificado") {
            monto = monto + paquete.total;
            const date = {
              year: moment(paquete.fecha).format('YYYY'),
              mes: moment(paquete.fecha).format('MM')
            };
            if (date.year === this.fecha) {
              if (paquete.forma_pago === 'Tarjeta') {
                if (date.mes === '01') {
                  this.emailSentBarChart.series[0].data[0] = this.emailSentBarChart.series[0].data[0] + paquete.total;
                }
                if (date.mes === '02') {
                  this.emailSentBarChart.series[0].data[1] = this.emailSentBarChart.series[0].data[1] + paquete.total;
                }
                if (date.mes === '03') {
                  this.emailSentBarChart.series[0].data[2] = this.emailSentBarChart.series[0].data[2] + paquete.total;
                }
                if (date.mes === '04') {
                  this.emailSentBarChart.series[0].data[3] = this.emailSentBarChart.series[0].data[3] + paquete.total;
                }
                if (date.mes === '05') {
                  this.emailSentBarChart.series[0].data[4] = this.emailSentBarChart.series[0].data[4] + paquete.total;
                }
                if (date.mes === '06') {
                  this.emailSentBarChart.series[0].data[5] = this.emailSentBarChart.series[0].data[5] + paquete.total;
                }
                if (date.mes === '07') {
                  this.emailSentBarChart.series[0].data[6] = this.emailSentBarChart.series[0].data[6] + paquete.total;
                }
                if (date.mes === '08') {
                  this.emailSentBarChart.series[0].data[7] = this.emailSentBarChart.series[0].data[7] + paquete.total;
                }
                if (date.mes === '09') {
                  this.emailSentBarChart.series[0].data[8] = this.emailSentBarChart.series[0].data[8] + paquete.total;
                }
                if (date.mes === '10') {
                  this.emailSentBarChart.series[0].data[9] = this.emailSentBarChart.series[0].data[9] + paquete.total;
                }
                if (date.mes === '11') {
                  this.emailSentBarChart.series[0].data[10] = this.emailSentBarChart.series[0].data[10] + paquete.total;
                }
                if (date.mes === '12') {
                  this.emailSentBarChart.series[0].data[11] = this.emailSentBarChart.series[0].data[11] + paquete.total;
                }
              } else {
                if (date.mes === '01') {
                  this.emailSentBarChart.series[1].data[0] = this.emailSentBarChart.series[1].data[0] + paquete.total;
                }
                if (date.mes === '02') {
                  this.emailSentBarChart.series[1].data[1] = this.emailSentBarChart.series[1].data[1] + paquete.total;
                }
                if (date.mes === '03') {
                  this.emailSentBarChart.series[1].data[2] = this.emailSentBarChart.series[1].data[2] + paquete.total;
                }
                if (date.mes === '04') {
                  this.emailSentBarChart.series[1].data[3] = this.emailSentBarChart.series[1].data[3] + paquete.total;
                }
                if (date.mes === '05') {
                  this.emailSentBarChart.series[1].data[4] = this.emailSentBarChart.series[1].data[4] + paquete.total;
                }
                if (date.mes === '06') {
                  this.emailSentBarChart.series[1].data[5] = this.emailSentBarChart.series[1].data[5] + paquete.total;
                }
                if (date.mes === '07') {
                  this.emailSentBarChart.series[1].data[6] = this.emailSentBarChart.series[1].data[6] + paquete.total;
                }
                if (date.mes === '08') {
                  this.emailSentBarChart.series[1].data[7] = this.emailSentBarChart.series[1].data[7] + paquete.total;
                }
                if (date.mes === '09') {
                  this.emailSentBarChart.series[1].data[8] = this.emailSentBarChart.series[1].data[8] + paquete.total;
                }
                if (date.mes === '10') {
                  this.emailSentBarChart.series[1].data[9] = this.emailSentBarChart.series[1].data[9] + paquete.total;
                }
                if (date.mes === '11') {
                  this.emailSentBarChart.series[1].data[10] = this.emailSentBarChart.series[1].data[10] + paquete.total;
                }
                if (date.mes === '12') {
                  this.emailSentBarChart.series[1].data[11] = this.emailSentBarChart.series[1].data[11] + paquete.total;
                }
              }
            }
          }
        }
        this.show = true;
        total = total + monto;
        this.statData[4].value = monto;
      //  console.log(this.emailSentBarChart);
        this.db.col$('Pago_multas').subscribe((multas: any) => {
          monto = 0;
          for (const multa of multas) {
            if (!multa.status) {
              monto = monto + multa.total;
            }
          }
          total = total + monto;
          this.statData[5].value = monto;
          this.statData[6].value = total;
          this.spinner.hide();



          
        }, error => {
          this.spinner.hide();
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Revisa tu conexión a internet',
          });
        });
        this.db.col$('Problemas').subscribe((problems: any) => {
          monto = 0;
          for (const problem of problems) {
            if (problem.status===true) {
              this.statData[7].value = +this.statData[7].value + 1;
            }
          
          }
         
          
        }, error => {
          this.spinner.hide();
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Revisa tu conexión a internet',
          });
        });

      }, error => {
        this.spinner.hide();
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: 'Revisa tu conexión a internet',
        });
      });
    }, error => {
      this.spinner.hide();
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Revisa tu conexión a internet',
      });
    });
    
  }
  

}
