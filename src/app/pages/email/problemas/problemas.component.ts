import { Component, EventEmitter, OnInit, ɵConsole } from '@angular/core';
import { Project } from './problemas.model';
import { DatabaseService } from '../../../services/database.service';
import Swal from 'sweetalert2';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
//import { Subcribe } from 'src/app/core/models/auth.models';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
import * as moment from 'moment/moment';
@Component({
  selector: 'app-problemas',
  templateUrl: './problemas.component.html',
  styleUrls: ['./problemas.component.scss']
})
export class ProblemasComponent implements OnInit {
  title = 'mat-tabs';
  // bread crumb items
  breadCrumbItems: Array<{}>;
  projectData2: Project[];
  projectData1: Project[];
  projectData: Project[];
  usuarios: Project[];
  usuarior: Project[];
  editdata: any;
  srcTengo = './assets/images/articulo.png';
  srcEntregado = 'c';
  newUser = {
    accion: 'Verificaciones',
    date: moment().format('YYYY-MM-DD'),
    idreceptor: '',
    idemisor: this.authenticated.currentUser.id,
    notificacion: ''
    // nombre: '',
    // password: '',
    // rol: '',
    // status: true
  };
  constructor(private authenticated: AuthenticatedService, private spinner: NgxSpinnerService, private modalService: NgbModal, private router: Router, private db: DatabaseService) { }
  ngOnInit() {
    this.init();

    this.breadCrumbItems = [{ label: 'Problemas' }, { label: 'Notificación de problemas', active: true }];

  }
  init() {
    this.spinner.show();

    this.db.col$('Problemas').subscribe((data: any) => {
      this.projectData = [];
      this.usuarios = [];
      this.usuarior = [];

      const datos = data;
      for (const data of datos) {
        if (data.status === true) {
          this.projectData.push(data);
          //USUARIOS
          // console.log("PROBLEMS", this.projectData);

          this.db.col$('Usuarios').subscribe((data2: any) => {
            this.usuarios = [];
            this.usuarior = [];
            const datos2 = data2;
            for (const data2 of datos2) {

              if (data.id_usuario_envia === data2.id) {
                this.usuarios.push(data2);
                this.spinner.hide();

              } if (data.id_usuario_recibe === data2.id) {
                this.usuarior.push(data2);
                this.spinner.hide();

              }
            }
          }, error => {


          }).add(() => {
            this.spinner.hide();
          });



        }
      }
    }, error => {


    }).add(() => {
      this.spinner.hide();
    });

  }



  //
  Actual() {

  }
  edit() {


  }

  //ABRIR MODAL
  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    this.editdata = user;


  }
  //CERRAR MODAL


  solucionar(data, id_usuario_envia, id_usuario_recibe, solucion, token_envia, token_recibe) {
    this.db.updated('Problemas/' + data, { status: false, solucion: solucion, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {

        this.db.sendNotification('Problema solucionado', 'Su problema ha sido solucionado, revisa tus notificaciones', token_envia).subscribe((response: any) => {
          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Problema',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: id_usuario_envia,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Solución: ' + solucion
            // nombre: '',
            // password: '',
            // rol: '',
            // status: true

          };
          this.db.add('Notificaciones', this.newUser).then(() => {
            // Swal.fire(
            //   'Correcto',
            //   'Administrador registrado',
            //   'success'
            // );
            // this.init();
            this.cerrar();

            console.log("receptor", id_usuario_envia);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });
        //RECIBE

        this.db.sendNotification('Problema solucionado', 'Su problema ha sido solucionado, revisa tus notificaciones', token_recibe).subscribe((response: any) => {
          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Problema',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: id_usuario_recibe,
            idemisor: this.authenticated.currentUser.id,
            notificacion: solucion
            // nombre: '',
            // password: '',
            // rol: '',
            // status: true

          };
          this.db.add('Notificaciones', this.newUser).then(() => {
            // Swal.fire(
            //   'Correcto',
            //   'Administrador registrado',
            //   'success'
            // );
            // this.init();
            this.cerrar();

            console.log("receptor", id_usuario_recibe);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });





        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Problema solucionado!',
          'success'
        )
        this.cerrar();
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        //this.ngOnInit();
        this.init();
        this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });



  }
  cerrar() {
    this.modalService.dismissAll();
  }
  solucion(data) {

  }
  bloquearUserEnvia(data, solucion, token_envia) {

    // this.db.updated('Usuarios/' + data, { bloqueado: true, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
    this.db.updated('Usuarios/' + data, { bloqueado: true, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {

        console.log("id user",data);
        // console.log("token1",token_envia);

        this.db.sendNotification('Bloqueo', 'El problema ha sido solucionado y usted ha sido bloqueado', token_envia).subscribe((response: any) => {

          // console.log("tokene2",token_envia);

          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Bloqueo',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Solución con bloqueo ' + solucion


          };
          this.db.add('Notificaciones', this.newUser).then(() => {


            console.log("receptor", data);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });








        // this.spinner.show();
        Swal.fire(
          'Correcto',
          'Usuario bloqueado!',
          'success'
        )
        // this.cerrar();
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        // this.init();
        //this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
  }

  bloquearUserEnviaRet(data, solucion, token_envia) {

    this.db.updated('Usuarios/' + data, { bloqueado: false, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {

        // console.log("id user",data);
        // console.log("token1",token_envia);

        this.db.sendNotification('Desbloqueo', 'El problema ha sido solucionado y usted ha sido desbloqueado', token_envia).subscribe((response: any) => {

          // console.log("tokene2",token_envia);

          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Desbloqueo',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Solución con desbloqueo ' + solucion


          };
          this.db.add('Notificaciones', this.newUser).then(() => {


            console.log("receptor", data);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });


        Swal.fire(
          'Correcto',
          'Usuario desbloqueado!',
          'success'
        )
        // this.cerrar();
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        // this.init();
        //this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
  }

  bloquearUserRecibe(data, solucion, token_recibe) {

    this.db.updated('Usuarios/' + data, { bloqueado: true, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {

        // console.log("id user",data);
        // console.log("token1",token_envia);

        this.db.sendNotification('Bloqueo', 'El problema ha sido solucionado y usted ha sido bloqueado', token_recibe).subscribe((response: any) => {

          // console.log("tokene2",token_envia);

          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Bloqueo',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Solución con bloqueo ' + solucion


          };
          this.db.add('Notificaciones', this.newUser).then(() => {


            console.log("receptor", data);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });



        Swal.fire(
          'Correcto',
          'Usuario bloqueado!',
          'success'
        )
        // this.cerrar();
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        // this.init();
        //this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
  }

  bloquearUserRecibeRet(data, solucion, token_recibe) {

    this.db.updated('Usuarios/' + data, { bloqueado: false, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {

        // console.log("id user",data);
        // console.log("token1",token_envia);

        this.db.sendNotification('Desbloqueo', 'El problema ha sido solucionado y usted ha sido desbloqueado', token_recibe).subscribe((response: any) => {

          // console.log("tokene2",token_envia);

          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Desbloqueo',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Solución con desbloqueo ' + solucion


          };
          this.db.add('Notificaciones', this.newUser).then(() => {


            console.log("receptor", data);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });

        Swal.fire(
          'Correcto',
          'Usuario desbloqueado!',
          'success'
        )
        // this.cerrar();
        //console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        // this.init();
        //this.spinner.hide();
        //  this.paquete();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
  }
  //solucion2(data){
  //   this.spinner.show();
  //   this.api.solucion2(data.id,data.solucion,data.bloqueoT,data.bloqueadoT,data.montoT,data.bloqueoW,data.bloqueadoW, data.montoW).subscribe((data)=>{


  //     Swal.fire(
  //      'Correcto!',
  //      'Solución ejecutada!',
  //      'success'
  //    )

  //    this.cerrar();
  //    this.init();
  //    //window.location.reload();

  //   },(error)=>{
  //     console.log(error);
  //   }).add (() => {
  //     this.spinner.hide();
  //    });

  // }

}