import { Component, OnInit } from '@angular/core';

import { Usergrid } from './usergrid.model';

import { userGridData } from './data';
import { DatabaseService } from '../../../services/database.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from '../../email/problemas/problemas.model';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import * as moment from 'moment/moment';
import { AuthenticatedService } from '../../../core/services/authenticated.service';
@Component({
  selector: 'app-usergrid',
  templateUrl: './usergrid.component.html',
  styleUrls: ['./usergrid.component.scss']
})

/**
 * Contacts user grid component
 */
export class UsergridComponent implements OnInit {
  // bread crumb items
  breadCrumbItems: Array<{}>;
  editdata: any;
  userGridData: Usergrid[];
  projectData: Project[];
  profesionales: Project[];
  usuarios: Project[];
  newUser = {
    accion: 'Verificaciones',
    date: moment().format('YYYY-MM-DD'),
    idreceptor: '',
    idemisor: this.authenticated.currentUser.id,
    notificacion: ''
    // nombre: '',
    // password: '',
    // rol: '',
    // status: true
  };
  constructor(
    private authenticated: AuthenticatedService,
    private db: DatabaseService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router) { }

  ngOnInit() {
    this.init();
    this.breadCrumbItems = [{ label: 'Verificaciones' }, { label: 'Documentos: Cédula', active: true }];

    /**
     * fetches data
     */
    this._fetchData();
  }

  init() {
    this.spinner.show();
    this.db.col$('Usuarios').subscribe((data: any) => {
      this.projectData = [];
      this.profesionales = [];
      this.usuarios = [];

      const datos = data;
      // console.log(datos);
      for (const data of datos) {
        if (data.verif_cedula === false && data.tipo === "profesional" && data.status) {
          this.profesionales.push(data);
          this.spinner.hide();
          // console.log("DATOS", data);
        } else if (data.verif_cedula === false && data.tipo === "usuario" && data.status) {
          this.usuarios.push(data);
          // console.log("DATOS", data);
          this.spinner.hide();
        }

      }

      // this.userGridData = data;
      // console.log(data);
    }, error => {


    }).add(() => {
      
    });
  }
  /**
   * User grid data fetches
   */
  private _fetchData() {
    this.userGridData = userGridData;
  }

  aprobar(data) {
    console.log(data);
    this.db.updated('Usuarios/' + data.id, { verif_cedula: true, id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD') })
      .then(() => {
        this.db.sendNotification('Correcto', 'Su cédula ha sido verificada', data.token).subscribe((response: any) => {
          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Verificación',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data.id,
            idemisor: this.authenticated.currentUser.id,
            notificacion: 'Se ha verificado correctamente su cédula'
            // nombre: '',
            // password: '',
            // rol: '',
            // status: true

          };
          this.db.add('Notificaciones', this.newUser).then(() => {
            // Swal.fire(
            //   'Correcto',
            //   'Administrador registrado',
            //   'success'
            // );
            // this.init();
            // this.cerrar();

            console.log("receptor", data.id);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });



        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Cédula verificada!',
          'success'
        )
        console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        this.init();
        this.spinner.hide();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
    console.log("RESULTADO", data);




  }


  Rechazar(data, negado, token) {
    this.db.updated('Usuarios/' + data, { id_user_action: this.authenticated.currentUser.id, fecha_modifica: moment().format('YYYY-MM-DD'), negado: negado, verif_cedula: false, status: false })
      .then(() => {
        this.db.sendNotification('Verificación negada', 'Su cédula no ha sido verificada, revise las notificaciones', token).subscribe((response: any) => {
          // GUARDAR DATOS
          this.newUser.date = moment().format('YYYY-MM-DD');
          this.newUser = {
            accion: 'Verificación',
            date: moment().format('YYYY-MM-DD HH:mm'),
            idreceptor: data,
            idemisor: this.authenticated.currentUser.id,
            notificacion: negado
            // nombre: '',
            // password: '',
            // rol: '',
            // status: true

          };
          this.db.add('Notificaciones', this.newUser).then(() => {
            // Swal.fire(
            //   'Correcto',
            //   'Administrador registrado',
            //   'success'
            // );
            // this.init();
            this.cerrar();

            console.log("receptor", data.id);

          }).catch(error => {
            this.spinner.hide();
            Swal.fire(
              'Error',
              'Revisa tu conexion a internet',
              'error'
            );
          });

        }, error => {
          console.error(error);
        });



        this.spinner.show();
        Swal.fire(
          'Correcto',
          'Verificación negada!',
          'success'
        )
        console.log("Actualizado")
        // this.router.navigate(['/projects/list']);
        this.init();
        this.spinner.hide();
      }
      )
      .catch(err => {
        Swal.fire({
          icon: 'error',
          title: 'ERROR',
          text: 'No se realizo la acción',
        });
        console.log("ERROR", err);
      });
    console.log("RESULTADO", data);




  }



  cerrar() {
    this.modalService.dismissAll();
  }
  openModal(content: any, user) {
    this.modalService.open(content, { centered: true });
    this.editdata = user;
  }
}
