import {Injectable} from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import {AngularFirestore,AngularFirestoreCollection, AngularFirestoreDocument} from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
type CollentionPredicate<T>= string | AngularFirestoreCollection;
type DocumentPredicate<T> = string | AngularFirestoreDocument;
@Injectable({
    providedIn: 'root'
})

export class DatabaseService{
    constructor( private afs: AngularFirestore,
                 private http: HttpClient){}
    private col<T>(ref: CollentionPredicate<T>, queryFn?): AngularFirestoreCollection{
        return typeof ref === 'string' ? this.afs.collection(ref, queryFn) : ref;

    }
    private doc<T>(ref: DocumentPredicate<T>): AngularFirestoreDocument{
        return typeof ref === 'string' ? this.afs.doc(ref) : ref;
    }
 

    private doc2<T>(ref: DocumentPredicate<T>,ref2: DocumentPredicate<T>): AngularFirestoreDocument{
        return typeof ref=== "string"? this.afs.doc(ref): ref,typeof ref2=== "string"? this.afs.doc(ref2): ref2;
    }

    add<T>(ref:CollentionPredicate<T>,data){
        return this.col(ref).add({
            ...data
        })
    }
    // col$<T>(ref:CollentionPredicate<T>,queryFn?):Observable<T[]>{
    //     return this.col(ref,queryFn).snapshotChanges().pipe(
    //         map(docs => {
    //             return docs.map(d =>d.payload.doc.data()) as T[];
    //         })
    //     )     
    // }  
    col$<T>(ref:CollentionPredicate<T>,queryFn?):Observable<any[]>{
        return this.col(ref,queryFn).snapshotChanges().pipe(
            map(docs => {
                return docs.map(d =>{
                    const data=d.payload.doc.data();
                    const id=d.payload.doc.id;
                    return{id,...data}    
                })
            })
        )     
    }
    updated<T>(ref: DocumentPredicate<T>, data){
        return this.doc(ref).update({
            ...data
        });
    }
    updated2<T>(ref: DocumentPredicate<T>, data){
        return this.doc(ref).update({
            ...data
        });
    }

    sendNotification(title,body, to) {
        const postUrl = 'https://fcm.googleapis.com/fcm/send';
        const data = {
            notification: {
                
                title,
                body            },
          priority: 'high',
          data: {
            click_action: 'FLUTTER_NOTIFICATION_CLICK',
            title,
            body,
          },
          to
        };
        const headers = {
            'content-type': 'application/json',
            Authorization: 'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
        };
        return this.http.post(postUrl, data, {headers});
    }
  
    

    
}
