export class User {
    id: number;
    username: string;
    password: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    email: string;
}
export class Subcribe {
    status: boolean;
    response?: string[];
    message?: string;
}
