import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedService {
  currentUser: any;
  authenticated = false;
  constructor(private router: Router) {
    this.isAuthenticated();
  }

  isAuthenticated() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) || null;
    if (currentUser) {
      this.currentUser = currentUser;
      return true;
    } else {
      return false;
    }
  }

  setCurrentUser(currentUser) {
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.currentUser = currentUser;
    this.authenticated = true;
  }

  logout(){
    this.currentUser = null,
    this.authenticated = false;
    localStorage.removeItem('currentUser');
    this.router.navigate(['/account/login']);
  }
}
