import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { Project } from '../../../pages/email/problemas/problemas.model';
import { DatabaseService } from '../../../services/database.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  @Input() transactions: Array<{
    id?: string;
    index?: number,
    name?: string,
    date?: string,
    total?: string,
    status?: string,
    payment?: string[],
  }>;
  projectData: Project[];
  editdata: any;
  constructor(private spinner: NgxSpinnerService, private modalService: NgbModal, private router: Router, private db: DatabaseService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.spinner.show();
    this.db.col$('Paymentez').subscribe((data: any) => {
      this.projectData = [];
      this.spinner.hide();
      const datos = data;
      for (const dato of datos){
        // VERIFICAR EL ESTADO ANTES DEL REVERSO
        // if (dato.status) {
          // this.db.col$('Paymentez').subscribe((data: any) => {
          this.projectData.push(dato);
        // }
      }
    }, error => {
      this.spinner.hide();
    });
  }
  reverso(payment) {
    console.log(payment);
  }
}
